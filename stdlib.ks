@lazyglobal off.
"kpp 1.0".
if defined Class {}else{
  global Class to lex(
    "prototype", lex(
      "__new", {
        parameter c.
        local p to c:prototype:copy.
        set p:type to c.
        return p.
      },
      "mix",
      {
        parameter s, m.
        for k in m:keys
          if not k:startsWith("__")
            set s[k] to m[k].
      }
    )
  ).
}
set Class:__new to {
  parameter s,b.
  local r to s:prototype:copy.
  set r:prototype to lex().
  b:call(r).
  return r.
}.
"https://gitlab.com/thexa4/kos-kpp".

// Formatted:
@lazyglobal off.
"kpp 1.1".if defined Class{}else{global Class to lexicon("prototype",lex("__new",{parameter c.local p to
c:prototype:copy. set p:type to c. return p.},"mix",{parameter s,m.
for k in m:keys if not k:startsWith("__")set s[k] to m[k].})).}set Class:__new to{parameter s,b.local r to
s:prototype:copy. set r:prototype to lex().b:call(r). return r.}."https://gitlab.com/thexa4/kos-kpp".