@lazyglobal off.

parameter infile to false.

declare function _lex {
	declare parameter input, filename is "unknown", callback is { declare parameter curstage, file, pos, max. }, noeof to false.

	local symbols to list(
		list("COMMENTLINE", "//[^\n]*", 0, {
			// Try matching bigger identifiers until it no longer works
			declare parameter input.
			local length to -1.
			local matches to true.
			until matches = false {
				set length to length + 1.
				local regex to "^//[^\n]{" + (length + 1) + "}".
				set matches to input:matchespattern(regex).
			}
			return length + 2.
		}),
		list("LAZYGLOBAL", "lazyglobal\b", 10, {}),
	
		//Math
		list("ACCESSOR", "->", 2, {}),
		list("PLUSMINUS", "(\+|-)", 1, {}),
		list("MULT", "\*", 1, {}),
		list("DIV", "/", 1, {}),
		list("POWER", "\^", 1, {}),
		list("E", "e((?=\d)|\b)", 1, {}),

		//Logic
		list("NOT", "not\b", 3, {}),
		list("AND", "and\b", 3, {}),
		list("OR", "or\b", 2, {}),
		list("TRUEFALSE", "(true\b|\bfalse\b)", 0, {
			declare parameter input.
			return choose 4 if input:startswith("t") else 5.
		}),
		list("COMPARATOR", "(<>|>=|<=|==|>|<)", 0, {
			declare parameter input.
			if input:startswith("<>") { return 2. }
			if input:startswith(">=") { return 2. }
			if input:startswith("<=") { return 2. }
			return 1.
		}),
		list("ASSIGNMENT", "=", 1, {}),

		//Instructions tokens
		list("SET", "set\b", 3, {}),
		list("TO", "to\b", 2, {}),
		list("IS", "is\b", 2, {}),
		list("IF", "if\b", 2, {}),
		list("ELSE", "else\b", 4, {}),
		list("UNTIL", "until\b", 5, {}),
		list("STEP", "step\b", 4, {}),
		list("DO", "do\b", 2, {}),
		list("LOCK", "lock\b", 4, {}),
		list("UNLOCK", "unlock\b", 6, {}),
		list("PRINT", "print\b", 5, {}),
		list("AT", "at\b", 2, {}),
		list("ON", "on\b", 2, {}),
		list("TOGGLE", "toggle\b", 6, {}),
		list("WAIT", "wait\b", 4, {}),
		list("WHEN", "when\b", 4, {}),
		list("THEN", "then\b", 4, {}),
		list("OFF", "off\b", 3, {}),
		list("STAGE", "stage\b", 5, {}),
		list("CLEARSCREEN", "clearscreen\b", 11, {}),
		list("ADD", "add\b", 3, {}),
		list("REMOVE", "remove\b", 6, {}),
		list("LOG", "log\b", 3, {}),
		list("BREAK", "break\b", 5, {}),
		list("PRESERVE", "preserve\b", 8, {}),
		list("DECLARE", "declare\b", 7, {}),
		list("DEFINED", "defined\b", 7, {}),
		list("LOCAL", "local\b", 5, {}),
		list("GLOBAL", "global\b", 6, {}),
		list("PARAMETER", "parameter\b", 9, {}),
		list("FUNCTION", "function\b", 8, {}),
		list("RETURN", "return\b", 6, {}),
		list("SWITCH", "switch\b", 6, {}),
		list("COPY", "copy\b", 4, {}),
		list("FROM", "from\b", 4, {}),
		list("RENAME", "rename\b", 6, {}),
		list("VOLUME", "volume\b", 6, {}),
		list("FILE", "file\b", 4, {}),
		list("DELETE", "delete\b", 6, {}),
		list("EDIT", "edit\b", 4, {}),
		list("RUN", "run\b", 3, {}),
		list("RUNPATH", "runpath\b", 7, {}),
		list("RUNONCEPATH", "runoncepath\b", 11, {}),
		list("ONCE", "once\b", 4, {}),
		list("COMPILE", "compile\b", 7, {}),
		list("LIST", "list\b", 4, {}),
		list("REBOOT", "reboot\b", 6, {}),
		list("SHUTDOWN", "shutdown\b", 8, {}),
		list("FOR", "for\b", 3, {}),
		list("UNSET", "unset\b", 5, {}),
		list("CHOOSE", "choose\b", 6, {}),
		list("MIX", "mix\b", 3, {}),

		//Generic
		list("INCLUDE", "#include\b", 8, {}),
		list("MIFNDEF", "#ifndef\b", 7, {}),
		list("MIFDEF", "#ifdef\b", 6, {}),
		list("MELSE", "#else\b", 6, {}),
		list("MENDIF", "#endif\b", 6, {}),
		list("MDEFINE", "#define\b", 7, {}),
		list("MUNDEF", "#undef\b", 6, {}),
		list("BRACKETOPEN", "\(", 1, {}),
		list("BRACKETCLOSE", "\)", 1, {}),
		list("CURLYOPEN", "\{", 1, {}),
		list("CURLYCLOSE", "\}", 1, {}),
		list("SQUAREOPEN", "\[", 1, {}),
		list("SQUARECLOSE", "\]", 1, {}),
		list("COMMA", ",", 1, {}),
		list("COLON", ":", 1, {}),
		list("IN", "in\b", 2, {}),
		list("ARRAYINDEX", "#", 1, {}),
		list("ALL", "all\b", 3, {}),
		list("CLASS", "class\b", 5, {}),
		list("FUNC", "func\b", 4, {}),
		list("INIT", "init\b", 4, {}),
		list("SELF", "self\b", 4, {}),
		list("VAR", "var\b", 3, {}),

		list("NEW", "new\b", 3, {}),
		list("IDENTIFIER", "[_\p{L}]\w*", 0, {
			// Try matching bigger identifiers until it no longer works
			declare parameter input.
			local length to -1.
			local matches to true.
			until matches = false {
				set length to length + 1.
				local regex to "^[_\p{L}]\w{" + (length + 1) + "}".
				set matches to input:matchespattern(regex).
			}
			return length + 1.
		}),
		list("FILEIDENT", "[_\p{L}]\w*(\.[_\p{L}]\w*)*", 0, {
			// Try matching bigger identifiers until it no longer works
			declare parameter input.
			local length to -1.
			local matches to true.
			until matches = false {
				set length to length + 1.
				local regex to "^[_\p{L}][\w\.\p{L}]{" + (length + 1) + "}".
				set matches to input:matchespattern(regex).
			}
			return length + 1.
		}),
		list("DOUBLE", "\d[_\d]*\.\d[_\d]*", 0, {
			declare parameter input.
			local prefixlength to -1.
			local matches to true.

			until matches = false {
				set prefixlength to prefixlength + 1.
				local regex to "^[_\d]{" + (prefixlength) + "}".
				set matches to input:matchespattern(regex).
			}
			local rest to input:substring(prefixlength, input:length - prefixlength).

			local postfixlength to -1.
			set matches to true.
			until matches = false {
				set postfixlength to postfixlength + 1.
				local regex to "^[_\d]{" + (postfixlength) + "}".
				set matches to rest:matchespattern(regex).
			}

			return prefixlength + postfixlength - 1.
		}),
		list("INTEGER", "\d[_\d]*", 0, {
			// Try matching bigger identifiers until it no longer works
			declare parameter input.
			local length to -1.
			local matches to true.
			until matches = false {
				set length to length + 1.
				local regex to "^\d[_\d]{" + (length + 1) + "}".
				set matches to input:matchespattern(regex).
			}
			return length + 1.
		}),
		list("STRING", "@?\" + char(34) + "(\" + char(34) + "\" + char(34) + "|[^\" + char(34) + "])*\" + char(34), 0, {
			declare parameter input.
			local iterator to input:iterator.
			iterator:next.

			local foundquote to false.
			local length to 0.
			until(false) {
				set length to length + 1.
				iterator:next.
				
				if (iterator:atend and not foundquote) {
					print "Assertion failure, unterminated string found.".
					return 99999.
				}
				if (foundquote) {
					if (iterator:atend or iterator:value <> char(34)) {
						return length.
					} else {
						set foundquote to false.
					}
				} else {
					if (iterator:value = char(34)) {
						set foundquote to true.
					}
				}
			}
		}),
		list("EOI", "(\.|;)", 1, {}),
		list("EOI", "(\.|;)", 1, {}),
		list("ATSIGN", "@", 1, {}),
		list("NEWLINE", "\n", 1, {}),
		list("WHITESPACE", "[^\S\n]+", 0, {
		    // Try matching bigger identifiers until it no longer works
			declare parameter input.
			local length to 0.
			local matches to true.
			until matches = false {
				set length to length + 1.
				local regex to "^[^\S\n]{" + (length + 1) + "}".
				set matches to input:matchespattern(regex).
			}
			return length.
		})
	).


	local result to list().
	local pointer to input.
	local lineNumber to 1.
	local charOffset to 1.
	local lineCount to input:split(char(10)):length.
	local curList to result.
	until pointer:length = 0 {
		local matched to false.
		local lastMatch to false.
		for symbol in symbols {
			if pointer:matchespattern("^" + symbol[1]) {
				set matched to true.
				local length to symbol[2].
				if length = 0 {
					set length to symbol[3](pointer).
				}
				if length = 0 {
					print("bug in " + symbol[0]).
					set length to 1.
				}
				local string to pointer:substring(0, length).
				set pointer to pointer:remove(0, length).
				
				set lastMatch to list(symbol[0], string, filename, lineNumber, charOffset).
				if curlist:length >= 100 {
					local newList to list().
					curList:add(newList).
					set curList to newList.
				}
				curList:add(lastMatch).
				break.
			}
		}
		if not matched {
			print("Unexpected character found in file " + filename + ":" + lineNumber + ", char " + charOffset + ": '" + pointer:substring(0, min(10, pointer:length)) + "'...").
			return false.
		} else {
			set charOffset to charOffset + lastMatch[1]:length.
			if (lastMatch[0] = "NEWLINE") {
				set lineNumber to lineNumber + 1.
				set charOffset to 1.
				callback("lexing", filename, lineNumber, lineCount).
			}
		}
	}
	if not noeof
		curList:add(list("EOF", "", filename, lineNumber, charOffset)).
	curList:add(false). // Prevents unwrapping last item.
	return result.
}

declare function test_lexer {
	print(_lex("@lazyglobal off.", "test1")).
	//print(lex(char(34) + "4222.16344." + char(34), "test2")).
}
//test_lexer().

local replaceWhitespace to {
	declare parameter node, type, replacement.
	
	local orig to false.
	local recurse to {
		declare parameter node.
		
		if not orig:istype("Boolean")
			return node.
		
		if node:istype("Boolean")
			return node.
		
		if node:type:name = "__literal" {
			local cpy to node:copy.
			set orig to cpy:whitespace.
			if type:istype("Boolean")
				set cpy:whitespace to list().
			else
				set cpy:whitespace to list(list(type, replacement, "<generated>", -1, -1)).
			return cpy.
		}
		
		return node:type:transform(node, recurse).
	}.
	
	return list(recurse(node), orig).
}.
local clearWhitespace to {
	declare parameter node.
	return replaceWhitespace(node, false, false)[0].
}.

declare function parse_definitions {
	local result to lexicon().

	local valid_identifiers to list(
		"IDENTIFIER", "NOT", "AND", "OR", "TRUEFALSE", "SET", "TO",
		"IS", "IF", "ELSE", "UNTIL", "STEP", "DO", "LOCK", "UNLOCK",
		"PRINT", "AT", "ON", "TOGGLE", "WAIT", "WHEN", "THEN", "OFF",
		"STAGE", "CLEARSCREEN", "ADD", "REMOVE", "LOG", "BREAK",
		"PRESERVE", "DECLARE", "DEFINED", "LOCAL", "GLOBAL", "PARAMETER",
		"FUNCTION", "RETURN", "SWITCH", "COPY", "FROM", "RENAME",
		"VOLUME", "FILE", "DELETE", "EDIT", "RUN", "RUNPATH",
		"RUNONCEPATH", "ONCE", "COMPILE", "LIST", "REBOOT", "SHUTDOWN",
		"FOR", "UNSET", "CHOOSE", "LAZYGLOBAL", "IN", "ALL", "CLASS",
		"FUNC", "INIT", "SELF", "VAR"
	).
	local skip_symbol to {
		declare parameter input.

		local curList to input[0].
		local pos to 0.
		local curSymbol to curlist[pos].
		if pos + 1 = curList:length {
			if curSymbol:istype("List") {
				set pos to 0.
				set curList to curSymbol.
				set curSymbol to curList[0].
			}
		}	
		if curSymbol:istype("Boolean") {
			return false.
		}
		until (curSymbol[0] <> "WHITESPACE" and curSymbol[0] <> "NEWLINE" and curSymbol[0] <> "COMMENTLINE") {
			set pos to pos + 1.
			set curSymbol to curList[pos].
			if pos + 1 = curList:length {
				if curSymbol:istype("List") {
					set pos to 0.
					set curList to curSymbol.
					set curSymbol to curList[0].
				}
			}
			if curSymbol:istype("Boolean") {
				return false.
			}
		}
		return list(curSymbol, list(curList:sublist(pos + 1, curlist:length - pos - 1), input[1])).
	}.

	local debug_next_symbol to {
		declare parameter input.
		local next to skip_symbol(input).
		local curSymbol to next[0].
		return curSymbol[0] + "{'" + curSymbol[1] + "'} at " + curSymbol[2] + ":" + curSymbol[3] + ", column " + curSymbol[4].
	}.
	// O(b), not sure if we can do better.
	local concat_one to {
		declare parameter a, b.
		local result to a:copy.
		for item in b {
			result:add(item).
		}
		return result.
	}.
	local concat to {
		declare parameter lists.
		if (lists:length = 0) {
			return list().
		}
		local cur to lists[0].
		local lists to lists:sublist(1, lists:length - 1).
		for list in lists {
			set cur to concat_one(cur, list).
		}
		return cur.
	}.
	local symbolize to {
		declare parameter list.
		local parts to list().
		for item in list {
			if not item:istype("Boolean") {
				parts:add(item:type:to_symbols(item)).
			}
		}
		
		return concat(parts).
	}.
	local _noexpansion to false.
	local one_of to {
		declare parameter input, definitions, symbols, recurse.
		
		for symbol in symbols {
			if symbol = "?"
				return list(false, input).
			local match to recurse(input, definitions, symbol).
			if (not match:istype("Boolean")) {
				if symbols = valid_identifiers and not _noexpansion {
					// definition expansion
					local identifier to match[0]:literal[1].
					if input[1]:defines:haskey(identifier) {
						local result to input[1]:defines[identifier].
						return list(result, match[1]).
					}
				}
				return match.
			}
			if match = true
				return true.
		}
		
		return false.
	}.
	local in_order to {
		declare parameter input, definitions, symbols, recurse, owner is "unknown", first_matches to true.
		
		local results to list().
		local rest to input.
		local first to first_matches.
		for symbol_in in symbols {
			local match to false.
			local symbol to symbol_in.
			local optional to false.
			if (symbol:istype("String") and symbol:startswith("?")) {
				set optional to true.
				set symbol to symbol:remove(0, 1).
			}
			if symbol:istype("List") {
				set match to one_of(rest, definitions, symbol, recurse).
			} else {
				set match to recurse(rest, definitions, symbol).
			}
			if (match:istype("Boolean")) {
				if (optional) {
					results:add(false).
				} else {
					if (not first) {
						if match
							return true.
						
						local expectation to symbol.
						if expectation:istype("List")
							set expectation to expectation:join(" or ").
					
						print("Failure while parsing " + owner + ", expected to find " + expectation + ". Instead I found " + debug_next_symbol(rest)).
						return true.
					}
					return match.
				}
			} else {
				set first to false.
				results:add(match[0]).
				set rest to match[1].
			}
		}
		
		return list(results, rest).
	}.
	local optional to {
		declare parameter input, definitions, symbol, recurse.
		local match to recurse(input, definitions, symbol).
		if match:istype("Boolean")
			return list(false, input).
		return match.
	}.

	// internally used literal type
	local literalParser to lexicon().
	literalParser:add("parse", {
		declare parameter input, definitions, recurse.
		return false.
	}).
	literalParser:add("to_symbols", {
		declare parameter self.
		local result to self:whitespace:copy.
		result:add(self:literal).
		return result.
	}).
	literalParser:add("transform", {
		declare parameter self, func.
		return self.
	}).
	result:add("__literal", literalParser).

	// number
	local numberParser to lexicon().
	numberParser:add("parse", {
		declare parameter input, definitions, recurse.
		local captured to recurse(input, definitions, "INTEGER").
		if (captured:istype("Boolean")) {
			set captured to recurse(input, definitions, "DOUBLE").
		}
		if (captured:istype("Boolean")) {
			return false.
		}

		local result to lexicon().
		result:add("type", numberParser).
		result:add("literal", captured[0]).
		return list(result, captured[1]).
	}).
	numberParser:add("to_symbols", {
		declare parameter self.
		return self:literal:type:to_symbols(self:literal).
	}).
	numberParser:add("transform", {
		declare parameter self, func.
		return lexicon(
			"type", self:type,
			"literal", func(self:literal)
		).
	}).
	result:add("number", numberParser).

	// sci_number
	local sciNumberParser to lexicon().
	sciNumberParser:add("parse", {
		declare parameter input, definitions, recurse.
		local number to recurse(input, definitions, "number").
		if (number:istype("Boolean")) {
			return false.
		}

		local result to lexicon().
		result:add("type", sciNumberParser).
		result:add("number", number[0]).
		result:add("eLiteral", false).
		result:add("plusMinus", false).
		result:add("integer", false).
		local restInput to number[1].

		local eLiteral to recurse(restInput, definitions, "E").
		if (not eLiteral:istype("Boolean")) {
			set restInput to eLiteral[1].
			set result:eLiteral to eLiteral[0].

			local plusMinusLiteral to recurse(restInput, definitions, "PLUSMINUS").
			if (not plusMinusLiteral:istype("Boolean")) {
				set result:plusMinus to plusMinusLiteral[0].
				set restInput to plusMinusLiteral[1].
			}

			local integerLiteral to recurse(restInput, definitions, "INTEGER").
			if (integerLiteral:istype("Boolean")) {
				print("Unexpected " + restInput[0][0] + " '" + restInput[0][1] + "' while parsing sci_number in file " + restInput[0][2] + ":" + restInput[0][3] + ", " + restInput[0][4]).
				return true.
			}
			
			set result:integer to integerLiteral[0].
			set restInput to integerLiteral[1].
		}

		return list(result, restInput).
	}).
	sciNumberParser:add("transform", {
		declare parameter self, func.
		return lexicon(
			"type", self:type,
			"number", func(self:number),
			"eLiteral", func(self:eLiteral),
			"plusMinus", func(self:plusMinus),
			"integer", func(self:integer)
		).
	}).
	sciNumberParser:add("to_symbols", {
		declare parameter self.
		local numberLiterals to self:number:type:to_symbols(self:number).
		local eLiterals to list().
		local plusLiterals to list().
		local integerLiterals to list().

		if (not self:eLiteral:istype("Boolean")) {
			set eLiterals to self:eLiteral:type:to_symbols(self:eLiteral).
		}
		if (not self:plusMinus:istype("Boolean")) {
			set plusLiterals to self:plusMinus:type:to_symbols(self:plusMinus).
		}
		if (not self:integer:istype("Boolean")) {
			set integerLiterals to self:integer:type:to_symbols(self:integer).
		}

		return concat(list(numberLiterals, eLiterals, plusLiterals, integerLiterals)).
	}).
	result:add("sci_number", sciNumberParser).

	// atom
	local atomParser to lexicon().
	atomParser:add("parse", {
		declare parameter input, definitions, recurse.

		local sci_number to recurse(input, definitions, "sci_number").
		if (not sci_number:istype("Boolean")) {
			return list(lexicon(
				"type", atomParser,
				"atom_type", "sci_number",
				"sci_number", sci_number[0]
			), sci_number[1]).
		}
		local truefalse to recurse(input, definitions, "TRUEFALSE").
		if (not truefalse:istype("Boolean")) {
			return list(lexicon(
				"type", atomParser,
				"atom_type", "truefalse",
				"truefalse", truefalse[0]
			), truefalse[1]).
		}
		local identifier to one_of(input, definitions, valid_identifiers, recurse).
		if (not identifier:istype("Boolean")) {
			return list(lexicon(
				"type", atomParser,
				"atom_type", "identifier",
				"identifier", identifier[0]
			), identifier[1]).
		}
		local expr to in_order(input, definitions, list("BRACKETOPEN", "expr", "BRACKETCLOSE"), recurse, "expr").
		if (not expr:istype("Boolean")) {
			// BRACKETOPEN expr BRACKETCLOSE
			
			return list(lexicon(
				"type", atomParser,
				"atom_type", "braces",
				"open", expr[0][0],
				"expr", expr[0][1],
				"close", expr[0][2]
			), expr[1]).
		}
		local fileident to recurse(input, definitions, "FILEIDENT").
		if (not fileident:istype("Boolean")) {
			return list(lexicon(
				"type", atomParser,
				"atom_type", "fileident",
				"fileident", fileident[0]
			), fileident[1]).
		}
		local string to recurse(input, definitions, "STRING").
		if (not string:istype("Boolean")) {
			return list(lexicon(
				"type", atomParser,
				"atom_type", "string",
				"string", string[0]
			), string[1]).
		}
		return false.
	}).
	atomParser:add("transform", {
		declare parameter self, func.
		if self:atom_type = "sci_number"
			return lexicon(
				"type", self:type,
				"atom_type", self:atom_type,
				"sci_number", func(self:sci_number)
			).
		if self:atom_type = "truefalse"
			return lexicon(
				"type", self:type,
				"atom_type", self:atom_type,
				"truefalse", func(self:truefalse)
			).
		if self:atom_type = "identifier"
			return lexicon(
				"type", self:type,
				"atom_type", self:atom_type,
				"identifier", func(self:identifier)
			).
		if self:atom_type = "braces"
			return lexicon(
				"type", self:type,
				"atom_type", self:atom_type,
				"open", func(self:open),
				"expr", func(self:expr),
				"close", func(self:close)
			).
		if self:atom_type = "fileident"
			return lexicon(
				"type", self:type,
				"atom_type", self:atom_type,
				"fileident", func(self:fileident)
			).
		if self:atom_type = "string"
			return lexicon(
				"type", self:type,
				"atom_type", self:atom_type,
				"string", func(self:string)
			).
		print("unknown atom type").
		return false.
	}).
	atomParser:add("to_symbols", {
		declare parameter self.
		if (self:atom_type = "sci_number") {
			return self:sci_number:type:to_symbols(self:sci_number).
		}
		if (self:atom_type = "truefalse") {
			return self:truefalse:type:to_symbols(self:truefalse).
		}
		if (self:atom_type = "identifier") {
			return self:identifier:type:to_symbols(self:identifier).
		}
		if (self:atom_type = "fileident") {
			return self:fileident:type:to_symbols(self:fileident).
		}
		if (self:atom_type = "string") {
			return self:string:type:to_symbols(self:string).
		}
		if (self:atom_type = "braces") {
			return symbolize(list(self:open, self:expr, self:close)).
		}
		print("missing type for atom to_symbol").
		return true.
	}).
	result:add("atom", atomParser).
	
	// array_trailer
	local arrayTrailerParser to lexicon().
	arrayTrailerParser:add("parse", {
		declare parameter input, definitions, recurse.
		//( (ARRAYINDEX (IDENTIFIER | INTEGER)) | (SQUAREOPEN expr SQUARECLOSE) );
		
		local result to lexicon("type", arrayTrailerParser).
		local rest to false.
		
		local array_index to recurse(input, definitions, "ARRAYINDEX").
		if (not array_index:istype("Boolean")) {
			local identifier to one_of(array_index[1], definitions, valid_identifiers, recurse).
			set result["index_type"] to "hash".
			if (not identifier:istype("Boolean")) {
				set result["hash"] to identifier[0].
				set rest to identifier[1].
			} else {
				local integer to recurse(array_index[1], definitions, "INTEGER").
				if (integer:istype("Boolean")) {
					print("Unexpected input while parsing array_index").
					return true.
				}
				set result["index"] to integer[0].
				set rest to integer[1].
			}
		} else {
			local squareopen to recurse(input, definitions, "SQUAREOPEN").
			if (squareopen:istype("Boolean")) {			
				return false.
			}
			set result["index_type"] to "square".
			set result["open"] to squareopen[0].
			
			local expr to recurse(squareopen[1], definitions, "expr").
			if (expr:istype("Boolean")) {
				print("Unexpected input while parsing array_index").
				return true.
			}
			set result["index"] to expr[0].
			
			local squareclose to recurse(expr[1], definitions, "SQUARECLOSE").
			if (squareclose:istype("Boolean")) {
				print("Unexpected input while parsing array_index").
				return true.
			}
			set result["close"] to squareclose[0].
			set rest to squareclose[1].
		}

		return list(result, rest).
	}).
	arrayTrailerParser:add("transform", {
		declare parameter self, func.
		if self:index_type = "hash"
			return lexicon(
				"type", self:type,
				"index_type", self:index_type,
				"hash", func(self:hash),
				"index", func(self:index)
			).
		if self:index_type = "square"
			return lexicon(
				"type", self:type,
				"index_type", self:index_type,
				"open", func(self:open),
				"index", func(self:index),
				"close", func(self:close)
			).
		print("Unknown array trailer type").
		return true.
	}).
	arrayTrailerParser:add("to_symbols", {
		declare parameter self.
		//( (ARRAYINDEX (IDENTIFIER | INTEGER)) | (SQUAREOPEN expr SQUARECLOSE) );
		
		if (self:index_type = "hash") {
			return symbolize(list(self:hash, self:index)).
		} else {
			return symbolize(list(self:open, self:index, self:close)).
		}
	}).
	result:add("array_trailer", arrayTrailerParser).
	
	// function_trailer
	local functionTrailerParser to lexicon().
	functionTrailerParser:add("parse", {
		declare parameter input, definitions, recurse.
		// (BRACKETOPEN arglist? BRACKETCLOSE) | ATSIGN;
		
		local result to lexicon("type", functionTrailerParser).
		
		local rest to false.
		
		local bracketOpen to recurse(input, definitions, "BRACKETOPEN").
		if (not bracketOpen:istype("Boolean")) {
			set result["open"] to bracketOpen[0].
			set result["trailer_type"] to "braces".
			local arglist to recurse(bracketOpen[1], definitions, "arglist").
			local arg_rest to bracketOpen[1].
			
			if (not arglist:istype("Boolean")) {
				set result["arglist"] to arglist[0].
				set arg_rest to arglist[1].
			} else {
				if arglist
					return true.
				set result["arglist"] to false.
			}
			
			local bracketClose to recurse(arg_rest, definitions, "BRACKETCLOSE").
			if (bracketClose:istype("Boolean")) {
				if not bracketClose
					print("Expected bracket close after function_trailer. Instead I found: " + debug_next_symbol(arg_rest)).
				return true.
			}
			set rest to bracketClose[1].
			set result["close"] to bracketClose[0].
		} else {
			if bracketOpen
				return true.
			local atsign to recurse(input, definitions, "ATSIGN").
			if (atsign:istype("Boolean")) {
				return false.
			}
			set result["trailer_type"] to "at".
			set result["at"] to atsign[0].
			set rest to atsign[1].
		}
		return list(result, rest).
	}).
	functionTrailerParser:add("transform", {
		declare parameter self, func.
		if self:trailer_type = "braces"
			return lexicon(
				"type", self:type,
				"trailer_type", self:trailer_type,
				"open", func(self:open),
				"arglist", func(self:arglist),
				"close", func(self:close)
			).
		if self:trailer_type = "at"
			return lexicon(
				"type", self:type,
				"trailer_type", self:trailer_type,
				"at", func(self:at)
			).
		print("Unknown trailer_type in function_trailer").
		return true.
	}).
	functionTrailerParser:add("to_symbols", {
		declare parameter self.
		
		if (self:trailer_type = "at") {
			return symbolize(list(self:at)).
		} else {
			if (self:arglist:istype("Boolean")) {
				return symbolize(list(self:open, self:close)).
			}
			return symbolize(list(self:open, self:arglist, self:close)).
		}
	}).
	result:add("function_trailer", functionTrailerParser).
	
	// suffixterm_trailer
	local suffixtermTrailerParser to lexicon().
	suffixtermTrailerParser:add("parse", {
		declare parameter input, definitions, recurse.
		// (function_trailer | array_trailer);
		
		local result to lexicon("type", suffixtermTrailerParser).
		
		local trailer to one_of(input, definitions, list("function_trailer", "array_trailer"), recurse).
		if (trailer:istype("Boolean"))
			return false.
		
		set result["trailer"] to trailer[0].
		return list(result, trailer[1]).
	}).
	suffixtermTrailerParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"trailer", func(self:trailer)
		).
	}).
	suffixtermTrailerParser:add("to_symbols", {
		declare parameter self.
		
		return self:trailer:type:to_symbols(self:trailer).
	}).
	result:add("suffixterm_trailer", suffixtermTrailerParser).
	
	// obj_construction
	local objConstructionParser to lexicon().
	objConstructionParser:add("parse", {
		declare parameter input, definitions, recurse.
		// NEW atom BRACKETOPEN args BRACKETCLOSE
		local result to lexicon("type", objConstructionParser).

		local parts to in_order(input, definitions, list("NEW", "atom", "BRACKETOPEN", "?arglist", "BRACKETCLOSE"), recurse, "obj_construction").

		if (parts:istype("Boolean"))
			return false.
		
		set result["new"] to parts[0][0].
		set result["expr"] to parts[0][1].
		set result["open"] to parts[0][2].
		set result["args"] to parts[0][3].
		set result["close"] to parts[0][4].
		return list(result, parts[1]).
	}).
	objConstructionParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"new", func(self:new),
			"expr", func(self:expr),
			"open", func(self:open),
			"args", func(self:args),
			"close", func(self:close)
		).
	}).
	objConstructionParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self:new, self:expr, self:open, self:args, self:close)).
	}).
	result:add("obj_construction", objConstructionParser).
	
	// suffixterm
	local suffixtermParser to lexicon().
	suffixtermParser:add("parse", {
		declare parameter input, definitions, recurse.
		// (atom | obj_construction) suffixterm_trailer*;
	
		local result to lexicon("type", suffixtermParser).
		local atom to one_of(input, definitions, list("obj_construction", "atom"), recurse).
		if (atom:istype("Boolean")) {
			return atom.
		}
		
		set result["atom"] to atom[0].
		
		local rest to atom[1].
		local lastmatch to recurse(rest, definitions, "suffixterm_trailer").
		local trailers to list().
		
		until (lastmatch:istype("Boolean")) {
			trailers:add(lastmatch[0]).
			set rest to lastmatch[1].
			set lastmatch to recurse(rest, definitions, "suffixterm_trailer").
		}
		
		set result["trailers"] to trailers.
		
		return list(result, rest).
	}).
	suffixtermParser:add("transform", {
		declare parameter self, func.
		local atomMapped to func(self:atom).
		local mapped to list().
		for item in self:trailers {
			mapped:add(func(item)).
		}
		return lexicon(
			"type", self:type,
			"atom", atomMapped,
			"trailers", mapped
		).
	}).
	suffixtermParser:add("to_symbols", {
		declare parameter self.
		
		local symbols to self:trailers:copy.
		symbols:insert(0, self:atom).
		
		return symbolize(symbols).
	}).
	result:add("suffixterm", suffixtermParser).
	
	// suffix_trailer
	local suffixTrailerParser to lexicon().
	suffixTrailerParser:add("parse", {
		declare parameter input, definitions, recurse.
		// (COLON suffixterm);
		
		local result to lexicon("type", suffixTrailerParser).
		local colon to recurse(input, definitions, "COLON").
		if (colon:istype("Boolean")) {
			return false.
		}
		set result["colon"] to colon[0].
		
		local suffixterm to recurse(colon[1], definitions, "suffixterm").
		if (suffixterm:istype("Boolean")) {
			print("Expected suffixterm while parsing suffix_trailer. Instead I found:" + debug_next_symbol(colon[1])).
			return true.
		}
		
		set result["suffixterm"] to suffixterm[0].
		
		return list(result, suffixterm[1]).
	}).
	suffixTrailerParser:add("transform", {
		declare parameter self, func.

		return lexicon(
			"type", self:type,
			"colon", func(self:colon),
			"suffixterm", func(self:suffixterm)
		).
	}).
	suffixTrailerParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self:colon, self:suffixterm)).
	}).
	result:add("suffix_trailer", suffixTrailerParser).
	
	// object_trailer
	local objectTrailerParser to lexicon().
	objectTrailerParser:add("parse", {
		declare parameter input, definitions, recurse.
		// ACCESSOR suffixterm
		
		local result to lexicon("type", objectTrailerParser).
		
		local args to in_order(input, definitions, list("ACCESSOR", "suffixterm"), recurse, "object_trailer").
		if (args:istype("Boolean")) {
			return args.
		}
		set result["accessor"] to args[0][0].
		set result["suffixterm"] to args[0][1].

		set result["unsafe"] to false.
		if args[1][1]:defines:haskey("UNSAFE_ACCESSOR")
			set result["unsafe"] to true.

		return list(result, args[1]).
	}).
	objectTrailerParser:add("transform", {
		declare parameter self, func.

		return lexicon(
			"type", self:type,
			"accessor", func(self:accessor),
			"suffixterm", func(self:suffixterm),
			"unsafe", self:unsafe
		).
	}).
	objectTrailerParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self:accessor, self:suffixterm)).
	}).
	result:add("object_trailer", objectTrailerParser).
	
	// suffix
	local suffixParser to lexicon().
	suffixParser:add("parse", {
		declare parameter input, definitions, recurse.
		// suffixterm (suffix_trailer | object_trailer)*;
		
		local result to lexicon("type", suffixParser).
		
		local suffixterm to recurse(input, definitions, "suffixterm").
		if (suffixterm:istype("Boolean")) {
			return suffixterm.
		}
		set result["suffixterm"] to suffixterm[0].
		
		
		local rest to suffixterm[1].
		local lastmatch to one_of(rest, definitions, list("suffix_trailer", "object_trailer"), recurse).
		local trailers to list().
		
		until (lastmatch:istype("Boolean")) {
			trailers:add(lastmatch[0]).
			set rest to lastmatch[1].
			
			set lastmatch to one_of(rest, definitions, list("suffix_trailer", "object_trailer"), recurse).
		}
		
		set result["trailers"] to trailers.
		
		return list(result, rest).
	}).
	suffixParser:add("transform", {
		declare parameter self, func.
		local newSuffix to func(self:suffixterm).
		local mapped to list().
		for item in self:trailers {
			mapped:add(func(item)).
		}
		return lexicon(
			"type", self:type,
			"suffixterm", newSuffix,
			"trailers", mapped
		).
	}).
	suffixParser:add("to_symbols", {
		declare parameter self.
		
		local symbols to self:trailers:copy.
		symbols:insert(0, self:suffixterm).
		
		return symbolize(symbols).
	}).
	result:add("suffix", suffixParser).

	// factor
	local factorParser to lexicon().
	factorParser:add("parse", {
		declare parameter input, definitions, recurse.
		// suffix (POWER suffix)*;
		
		local result to lexicon("type", factorParser).
		
		local suffix to recurse(input, definitions, "suffix").
		if (suffix:istype("Boolean")) {
			return suffix.
		}
		set result["suffix"] to suffix[0].
		
		
		local rest to suffix[1].
		local lastmatch to recurse(rest, definitions, "POWER").
		local powers to list().
		
		until (lastmatch:istype("Boolean")) {
			local inner_suffix to recurse(lastmatch[1], definitions, "suffix").
			if (inner_suffix:istype("Boolean")) {
				print("Expected suffix when parsing factor.").
				return true.
			}
		
			powers:add(list(lastmatch[0], inner_suffix[0])).
			set rest to inner_suffix[1].
			
			set lastmatch to recurse(rest, definitions, "POWER").
		}
		
		set result["powers"] to powers.
		
		return list(result, rest).
	}).
	factorParser:add("transform", {
		declare parameter self, func.
		local suffixMapped to func(self:suffix).
		local mapped to list().
		for item in self:powers {
			mapped:add(list(func(item[0]), func(item[1]))).
		}
		return lexicon(
			"type", self:type,
			"suffix", suffixMapped,
			"powers", mapped
		).
	}).
	factorParser:add("to_symbols", {
		declare parameter self.
		
		local symbols to list(self:suffix).
		for pair in self:powers {
			symbols:add(pair[0]).
			symbols:add(pair[1]).
		}
		
		return symbolize(symbols).
	}).
	result:add("factor", factorParser).
	
	// unary_expr
	local unaryExprParser to lexicon().
	unaryExprParser:add("parse", {
		declare parameter input, definitions, recurse.
		// (PLUSMINUS|NOT|DEFINED)? factor;
		
		local result to lexicon("type", unaryExprParser).
		
		local operator to one_of(input, definitions, list("PLUSMINUS", "NOT", "DEFINED"), recurse).
		local rest to input.
		if (operator:istype("Boolean")) {
			set result["operator"] to false.
		} else {
			set result["operator"] to operator[0].
			set rest to operator[1].
		}
		
		local factor to recurse(rest, definitions, "factor").
		if (factor:istype("Boolean")) {
			if (not operator:istype("Boolean")) {
				print("Expected factor while parsing unary_expr").
				return true.
			}
			return factor.
		}
		set result["operand"] to factor[0].
		
		return list(result, factor[1]).
	}).
	unaryExprParser:add("transform", {
		declare parameter self, func.

		return lexicon(
			"type", self:type,
			"operator", func(self:operator),
			"operand", func(self:operand)
		).
	}).
	unaryExprParser:add("to_symbols", {
		declare parameter self.
		
		if (self:operator:istype("Boolean")) {
			return symbolize(list(self:operand)).
		}
		
		return symbolize(list(self:operator, self:operand)).
	}).
	result:add("unary_expr", unaryExprParser).
	
	// multdiv_expr
	local multdivExprParser to lexicon().
	multdivExprParser:add("parse", {
		declare parameter input, definitions, recurse.
		// unary_expr ((MULT|DIV) unary_expr)*;
		
		local result to lexicon("type", multdivExprParser).
		
		local left to recurse(input, definitions, "unary_expr").
		if (left:istype("Boolean")) {
			return left.
		}
		set result["left"] to left[0].
		
		local rest to left[1].
		local lastmatch to one_of(rest, definitions, list("MULT", "DIV"), recurse).
		local parts to list().
		
		until (lastmatch:istype("Boolean")) {
			local inner_expr to recurse(lastmatch[1], definitions, "unary_expr").
			if (inner_expr:istype("Boolean")) {
				print("Expected unary_expr when parsing multdiv_expr.").
				return true.
			}
		
			parts:add(list(lastmatch[0], inner_expr[0])).
			set rest to inner_expr[1].
			
			set lastmatch to one_of(rest, definitions, list("MULT", "DIV"), recurse).
		}
		
		set result["parts"] to parts.
		
		return list(result, rest).
	}).
	multdivExprParser:add("transform", {
		declare parameter self, func.
		local mapped to list().
		for item in self:parts {
			mapped:add(list(func(item[0]), func(item[1]))).
		}
		return lexicon(
			"type", self:type,
			"left", func(self:left),
			"parts", mapped
		).
	}).
	multdivExprParser:add("to_symbols", {
		declare parameter self.
		
		local parts to list(self:left).
		for part in self:parts {
			parts:add(part[0]).
			parts:add(part[1]).
		}
		
		return symbolize(parts).
	}).
	result:add("multdiv_expr", multdivExprParser).
	
	// arith_expr
	local arithExprParser to lexicon().
	arithExprParser:add("parse", {
		declare parameter input, definitions, recurse.
		// multdiv_expr (PLUSMINUS multdiv_expr)*;
		
		local result to lexicon("type", arithExprParser).
		
		local left to recurse(input, definitions, "multdiv_expr").
		if (left:istype("Boolean")) {
			return left.
		}
		set result["left"] to left[0].
		
		local rest to left[1].
		local lastmatch to recurse(rest, definitions, "PLUSMINUS").
		local parts to list().
		
		until (lastmatch:istype("Boolean")) {
			local inner_expr to recurse(lastmatch[1], definitions, "multdiv_expr").
			if (inner_expr:istype("Boolean")) {
				print("Expected unary_expr when parsing multdiv_expr. Instead I found: " + debug_next_symbol(lastmatch[1])).
				return inner_expr.
			}
		
			parts:add(list(lastmatch[0], inner_expr[0])).
			set rest to inner_expr[1].
			
			set lastmatch to recurse(rest, definitions, "PLUSMINUS").
		}
		
		set result["parts"] to parts.
		
		return list(result, rest).
	}).
	arithExprParser:add("transform", {
		declare parameter self, func.
		local leftMapped to func(self:left).
		local mapped to list().
		for item in self:parts {
			mapped:add(list(func(item[0]), func(item[1]))).
		}
		return lexicon(
			"type", self:type,
			"left", leftMapped,
			"parts", mapped
		).
	}).
	arithExprParser:add("to_symbols", {
		declare parameter self.
		
		local parts to list(self:left).
		for part in self:parts {
			parts:add(part[0]).
			parts:add(part[1]).
		}
		
		return symbolize(parts).
	}).
	result:add("arith_expr", arithExprParser).

	// compare_expr
	local compareExprParser to lexicon().
	compareExprParser:add("parse", {
		declare parameter input, definitions, recurse.
		// arith_expr (COMPARATOR arith_expr)*;
		
		local result to lexicon("type", compareExprParser).
		
		local left to recurse(input, definitions, "arith_expr").
		if (left:istype("Boolean")) {
			return left.
		}
		set result["left"] to left[0].
		
		local rest to left[1].
		local lastmatch to one_of(rest, definitions, list("COMPARATOR", "ASSIGNMENT"), recurse).
		local parts to list().
		
		until (lastmatch:istype("Boolean")) {
			local inner_expr to recurse(lastmatch[1], definitions, "arith_expr").
			if (inner_expr:istype("Boolean")) {
				print("Expected unary_expr when parsing compare_expr. Instead I found: " + debug_next_symbol(lastmatch[1])).
				return inner_expr.
			}
		
			parts:add(list(lastmatch[0], inner_expr[0])).
			set rest to inner_expr[1].
			
			set lastmatch to one_of(rest, definitions, list("COMPARATOR", "ASSIGNMENT"), recurse).
		}
		
		set result["parts"] to parts.
		
		return list(result, rest).
	}).
	compareExprParser:add("transform", {
		declare parameter self, func.
		local leftMapped to func(self:left).
		local mapped to list().
		for item in self:parts {
			mapped:add(list(func(item[0]), func(item[1]))).
		}
		return lexicon(
			"type", self:type,
			"left", leftMapped,
			"parts", mapped
		).
	}).
	compareExprParser:add("to_symbols", {
		declare parameter self.
		
		local parts to list(self:left).
		for part in self:parts {
			parts:add(part[0]).
			parts:add(part[1]).
		}
		
		return symbolize(parts).
	}).
	result:add("compare_expr", compareExprParser).

	// and_expr
	local andExprParser to lexicon().
	andExprParser:add("parse", {
		declare parameter input, definitions, recurse.
		// compare_expr (AND compare_expr)*;
		
		local result to lexicon("type", andExprParser).
		
		local left to recurse(input, definitions, "compare_expr").
		if (left:istype("Boolean")) {
			return left.
		}
		set result["left"] to left[0].
		
		local rest to left[1].
		local lastmatch to recurse(rest, definitions, "AND").
		local parts to list().
		
		until (lastmatch:istype("Boolean")) {
			local inner_expr to recurse(lastmatch[1], definitions, "compare_expr").
			if (inner_expr:istype("Boolean")) {
				print("Expected unary_expr when parsing compare_expr.").
				return true.
			}
		
			parts:add(list(lastmatch[0], inner_expr[0])).
			set rest to inner_expr[1].
			
			set lastmatch to recurse(rest, definitions, "AND").
		}
		
		set result["parts"] to parts.
		
		return list(result, rest).
	}).
	andExprParser:add("transform", {
		declare parameter self, func.
		local leftMapped to func(self:left).
		local mapped to list().
		for item in self:parts {
			mapped:add(list(func(item[0]), func(item[1]))).
		}
		return lexicon(
			"type", self:type,
			"left", leftMapped,
			"parts", mapped
		).
	}).
	andExprParser:add("to_symbols", {
		declare parameter self.
		
		local parts to list(self:left).
		for part in self:parts {
			parts:add(part[0]).
			parts:add(part[1]).
		}
		
		return symbolize(parts).
	}).
	result:add("and_expr", andExprParser).
	
	// or_expr
	local orExprParser to lexicon().
	orExprParser:add("parse", {
		declare parameter input, definitions, recurse.
		// and_expr (OR and_expr)*;
		
		local result to lexicon("type", orExprParser).
		
		local left to recurse(input, definitions, "and_expr").
		if (left:istype("Boolean")) {
			return left.
		}
		set result["left"] to left[0].
		
		local rest to left[1].
		local lastmatch to recurse(rest, definitions, "OR").
		local parts to list().
		
		until (lastmatch:istype("Boolean")) {
			local inner_expr to recurse(lastmatch[1], definitions, "and_expr").
			if (inner_expr:istype("Boolean")) {
				print("Expected unary_expr when parsing and_expr.").
				return true.
			}
		
			parts:add(list(lastmatch[0], inner_expr[0])).
			set rest to inner_expr[1].
			
			set lastmatch to recurse(rest, definitions, "OR").
		}
		
		set result["parts"] to parts.
		
		return list(result, rest).
	}).
	orExprParser:add("transform", {
		declare parameter self, func.
		local leftMapped to func(self:left).
		local mapped to list().
		for item in self:parts {
			mapped:add(list(func(item[0]), func(item[1]))).
		}
		return lexicon(
			"type", self:type,
			"left", leftMapped,
			"parts", mapped
		).
	}).
	orExprParser:add("to_symbols", {
		declare parameter self.
		
		local parts to list(self:left).
		for part in self:parts {
			parts:add(part[0]).
			parts:add(part[1]).
		}
		
		return symbolize(parts).
	}).
	result:add("or_expr", orExprParser).
	
	// ternary_expr
	local ternaryExprParser to lexicon().
	ternaryExprParser:add("parse", {
		declare parameter input, definitions, recurse.
		// CHOOSE expr IF expr ELSE expr;
		
		local result to lexicon("type", ternaryExprParser).
		local parts to in_order(input, definitions, list("CHOOSE", "expr", "IF", "expr", "ELSE", "expr"), recurse, "ternary_expr").
		if (parts:istype("Boolean")) {
			return parts.
		}
		set result["choose"] to parts[0][0].
		set result["trueExp"] to parts[0][1].
		set result["if"] to parts[0][2].
		set result["question"] to parts[0][3].
		set result["else"] to parts[0][4].
		set result["falseExp"] to parts[0][5].
		
		return list(result, parts[1]).
	}).
	ternaryExprParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"choose", func(self:choose),
			"trueExp", func(self:trueExp),
			"if", func(self:if),
			"question", func(self:question),
			"else", func(self:else),
			"falseExp", func(self:falseExp)
		).
	}).
	ternaryExprParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self:choose, self["trueExp"], self["if"], self:question, self["else"], self["falseExp"])).
	}).
	result:add("ternary_expr", ternaryExprParser).

	// expr
	local exprParser to lexicon().
	exprParser:add("parse", {
		declare parameter input, definitions, recurse.
		// (ternary_expr|or_expr|instruction_block);
		
		local result to lexicon("type", exprParser).
		local inner to one_of(input, definitions, list("ternary_expr", "or_expr", "instruction_block"), recurse).
		if (inner:istype("Boolean")) {
			return inner.
		}
		set result["inner"] to inner[0].
		
		return list(result, inner[1]).
	}).
	exprParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"inner", func(self:inner)
		).
	}).
	exprParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self:inner)).
	}).
	result:add("expr", exprParser).
	
	// arglist
	local arglistParser to lexicon().
	arglistParser:add("parse", {
		declare parameter input, definitions, recurse.
		//  expr (COMMA expr)*;
		
		local result to lexicon("type", arglistParser).
		
		local left to recurse(input, definitions, "expr").
		if (left:istype("Boolean")) {
			return left.
		}
		set result["left"] to left[0].
		
		local rest to left[1].
		local lastmatch to recurse(rest, definitions, "COMMA").
		local parts to list().
		
		until (lastmatch:istype("Boolean")) {
			local inner_expr to recurse(lastmatch[1], definitions, "expr").
			if (inner_expr:istype("Boolean")) {
				if not inner_expr
					print("Expected expr when parsing arglist. Instead I found: " + debug_next_symbol(lastmatch[1])).
				return true.
			}
		
			parts:add(list(lastmatch[0], inner_expr[0])).
			set rest to inner_expr[1].
			
			set lastmatch to recurse(rest, definitions, "COMMA").
		}
		if lastmatch
			return true.
		
		set result["parts"] to parts.
		
		return list(result, rest).
	}).
	arglistParser:add("transform", {
		declare parameter self, func.
		local leftMapped to func(self:left).
		local mapped to list().
		for item in self:parts {
			mapped:add(list(func(item[0]), func(item[1]))).
		}
		return lexicon(
			"type", self:type,
			"left", leftMapped,
			"parts", mapped
		).
	}).
	arglistParser:add("to_symbols", {
		declare parameter self.
		
		local parts to list(self:left).
		for part in self:parts {
			parts:add(part[0]).
			parts:add(part[1]).
		}
		
		return symbolize(parts).
	}).
	result:add("arglist", arglistParser).
	
	// varidentifier
	local varidentifierParser to lexicon().
	varidentifierParser:add("parse", {
		declare parameter input, definitions, recurse.
		// suffix;
		
		local result to lexicon("type", varidentifierParser).
		
		local suffix to recurse(input, definitions, "suffix").
		if (suffix:istype("Boolean")) {
			return suffix.
		}
		set result["suffix"] to suffix[0].
		
		return list(result, suffix[1]).
	}).
	varidentifierParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"suffix", func(self:suffix)
		).
	}).
	varidentifierParser:add("to_symbols", {
		declare parameter self.
		
		return self:suffix:type:to_symbols(self:suffix).
	}).
	result:add("varidentifier", varidentifierParser).
	
	// identifier_led_expr
	local identifierLedExprParser to lexicon().
	identifierLedExprParser:add("parse", {
		declare parameter input, definitions, recurse.
		// suffix (onoff_trailer)?;
		
		local result to lexicon("type", identifierLedExprParser).
		
		local suffix to recurse(input, definitions, "suffix").
		if (suffix:istype("Boolean")) {
			return suffix.
		}
		set result["suffix"] to suffix[0].
		
		local rest to suffix[1].
		local onoffTrailer to recurse(suffix[1], definitions, "onoff_trailer").
		if (onoffTrailer:istype("Boolean")) {
			set result["trailer"] to false.
		} else {
			set result["trailer"] to onoffTrailer[0].
			set rest to onoffTrailer[1].
		}
		
		return list(result, rest).
	}).
	identifierLedExprParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"suffix", func(self:suffix),
			"trailer", func(self:trailer)
		).
	}).
	identifierLedExprParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self:suffix, self:trailer)).
	}).
	result:add("identifier_led_expr", identifierLedExprParser).
	
	// var_definition
	local varDefinitionParser to lexicon().
	varDefinitionParser:add("parse", {
		declare parameter input, definitions, recurse.
		// VAR identifier ASSIGNMENT expr EOI;
		
		local result to lexicon("type", varDefinitionParser).

		local parts to in_order(input, definitions, list("VAR", valid_identifiers, "ASSIGNMENT", "expr", "EOI"), recurse, "var_definition").
		if (parts:istype("Boolean")) {
			return parts.
		}
		set result["var"] to parts[0][0].
		set result["identifier"] to parts[0][1].
		set result["assignment"] to parts[0][2].
		set result["expr"] to parts[0][3].
		set result["eoi"] to parts[0][4].
		
		return list(result, parts[1]).
	}).
	varDefinitionParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"var", func(self:var),
			"identifier", func(self:identifier),
			"assignment", func(self:assignment),
			"expr", func(self:expr),
			"eoi", func(self:eoi)
		).
	}).
	varDefinitionParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self:var, self:identifier, self:assignment, self:expr, self:eoi)).
	}).
	result:add("var_definition", varDefinitionParser).
	
	// assignment_stmt
	local assignmentStatementParser to lexicon().
	assignmentStatementParser:add("parse", {
		declare parameter input, definitions, recurse.
		// varidentifier ASSIGNMENT expr EOI;
		
		local result to lexicon("type", assignmentStatementParser).

		local varidentifier to recurse(input, definitions, "varidentifier").
		if varidentifier:istype("Boolean")
			return varidentifier.
		set result["varidentifier"] to varidentifier[0].
		
		local parts to in_order(varidentifier[1], definitions, list("ASSIGNMENT", "expr", "EOI"), recurse, "assignment_stmt").
		if (parts:istype("Boolean")) {
			return parts.
		}
		set result["assignment"] to parts[0][0].
		set result["expr"] to parts[0][1].
		set result["eoi"] to parts[0][2].
		
		return list(result, parts[1]).
	}).
	assignmentStatementParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"varidentifier", func(self:varidentifier),
			"assignment", func(self:assignment),
			"expr", func(self:expr),
			"eoi", func(self:eoi)
		).
	}).
	assignmentStatementParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self:varidentifier, self:assignment, self:expr, self:eoi)).
	}).
	result:add("assignment_stmt", assignmentStatementParser).
	
	// identifier_led_stmt
	local identifierLedStatementParser to lexicon().
	identifierLedStatementParser:add("parse", {
		declare parameter input, definitions, recurse.
		// identifier_led_expr EOI;
		
		local result to lexicon("type", identifierLedStatementParser).
		
		local expr to recurse(input, definitions, "identifier_led_expr").
		if (expr:istype("Boolean")) {
			return expr.
		}
		set result["expr"] to expr[0].
		
		local eoi to recurse(expr[1], definitions, "EOI").
		if (eoi:istype("Boolean")) {
			if not eoi
				print("Expected '.' when parsing identifier_led_stmt. Instead I found: " + debug_next_symbol(expr[1])).
			return true.
		}
		set result["eoi"] to eoi[0].
		
		return list(result, eoi[1]).
	}).
	identifierLedStatementParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"expr", func(self:expr),
			"eoi", func(self:eoi)
		).
	}).
	identifierLedStatementParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self:expr, self:eoi)).
	}).
	result:add("identifier_led_stmt", identifierLedStatementParser).
	
	// empty_stmt
	local emptyStatementParser to lexicon().
	emptyStatementParser:add("parse", {
		declare parameter input, definitions, recurse.
		// EOI;
		
		local result to lexicon("type", emptyStatementParser).

		local eoi to recurse(input, definitions, "EOI").
		if (eoi:istype("Boolean")) {
			return eoi.
		}
		set result["eoi"] to eoi[0].
		
		return list(result, eoi[1]).
	}).
	emptyStatementParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"eoi", func(self:eoi)
		).
	}).
	emptyStatementParser:add("to_symbols", {
		declare parameter self.
		
		return self:eoi:type:to_symbols(self:eoi).
	}).
	result:add("empty_stmt", emptyStatementParser).
	
	// set_stmt
	local setStatementParser to lexicon().
	setStatementParser:add("parse", {
		declare parameter input, definitions, recurse.
		// SET varidentifier TO expr EOI;
		
		local result to lexicon("type", setStatementParser).

		local parts to in_order(input, definitions, list("SET", "varidentifier", "TO", "expr", "EOI"), recurse, "set_stmt").
		if (parts:istype("Boolean")) {
			return parts.
		}
		set result["set"] to parts[0][0].
		set result["varidentifier"] to parts[0][1].
		set result["to"] to parts[0][2].
		set result["expr"] to parts[0][3].
		set result["eoi"] to parts[0][4].
		
		return list(result, parts[1]).
	}).
	setStatementParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"set", func(self:set),
			"varidentifier", func(self:varidentifier),
			"to", func(self:to),
			"expr", func(self:expr),
			"eoi", func(self:eoi)
		).
	}).
	setStatementParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self:set, self:varidentifier, self:to, self:expr, self:eoi)).
	}).
	result:add("set_stmt", setStatementParser).
	
	// if_stmt
	local ifStatementParser to lexicon().
	ifStatementParser:add("parse", {
		declare parameter input, definitions, recurse.
		// IF expr instruction EOI? (ELSE instruction EOI?)?
		
		local result to lexicon("type", ifStatementParser).

		local prefix to in_order(input, definitions, list("IF", "expr", "instruction"), recurse, "if_stmt").
		if (prefix:istype("Boolean")) {
			return prefix.
		}
		set result["if"] to prefix[0][0].
		set result["expr"] to prefix[0][1].
		set result["true"] to prefix[0][2].
		set result["eoi1"] to false.
		
		local rest to prefix[1].
		local eoi1 to recurse(rest, definitions, "EOI").
		if (not eoi1:istype("Boolean")) {
			set result["eoi1"] to eoi1[0].
			set rest to eoi1[1].
		}
		
		set result["else"] to false.
		set result["false"] to false.
		local postfix to in_order(rest, definitions, list("ELSE", "instruction"), recurse, "if_stmt").
		if (not postfix:istype("Boolean")) {
			set result["else"] to postfix[0][0].
			set result["false"] to postfix[0][1].
			set rest to postfix[1].
		}
		
		set result["eoi2"] to false.
		local eoi2 to recurse(rest, definitions, "EOI").
		if (not eoi2:istype("Boolean")) {
			set result["eoi2"] to eoi2[0].
			set rest to eoi2[1].
		}
		
		return list(result, rest).
	}).
	ifStatementParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"if", func(self:if),
			"expr", func(self:expr),
			"true", func(self["true"]),
			"eoi1", func(self:eoi1),
			"else", func(self:else),
			"false", func(self["false"]),
			"eoi2", func(self:eoi2)
		).
	}).
	ifStatementParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self:if, self:expr, self["true"], self:eoi1, self:else, self["false"], self:eoi2)).
	}).
	result:add("if_stmt", ifStatementParser).
	
	// until_stmt
	local untilStatementParser to lexicon().
	untilStatementParser:add("parse", {
		declare parameter input, definitions, recurse.
		// UNTIL expr instruction EOI?;
		
		local result to lexicon("type", untilStatementParser).

		local parts to in_order(input, definitions, list("UNTIL", "expr", "instruction"), recurse, "until_stmt").
		if (parts:istype("Boolean")) {
			return parts.
		}
		set result["until"] to parts[0][0].
		set result["expr"] to parts[0][1].
		set result["instruction"] to parts[0][2].
		set result["eoi"] to false.
		
		local rest to parts[1].
		local eoi to recurse(rest, definitions, "EOI").
		if (not eoi:istype("Boolean")) {
			set result["eoi"] to eoi[0].
			set rest to eoi[1].
		}
		
		return list(result, rest).
	}).
	untilStatementParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"until", func(self:until),
			"expr", func(self:expr),
			"instruction", func(self:instruction),
			"eoi", func(self:eoi)
		).
	}).
	untilStatementParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self:until, self:expr, self:instruction, self:eoi)).
	}).
	result:add("until_stmt", untilStatementParser).
	
	// fromloop_stmt
	local fromloopStatementParser to lexicon().
	fromloopStatementParser:add("parse", {
		declare parameter input, definitions, recurse.
		// FROM instruction_block UNTIL expr STEP instruction_block DO instruction EOI?;
		
		local result to lexicon("type", fromloopStatementParser).

		local parts to in_order(input, definitions, list("FROM", "instruction_block", "UNTIL", "expr", "STEP", "instruction_block", "DO", "instruction"), recurse, "fromloop_stmt").
		if (parts:istype("Boolean")) {
			return parts.
		}
		set result["from"] to parts[0][0].
		set result["from_block"] to parts[0][1].
		set result["until"] to parts[0][2].
		set result["until_expr"] to parts[0][3].
		set result["step"] to parts[0][4].
		set result["step_block"] to parts[0][5].
		set result["do"] to parts[0][6].
		set result["do_instruction"] to parts[0][7].
		set result["eoi"] to false.
		
		local rest to parts[1].
		local eoi to recurse(rest, definitions, "EOI").
		if (not eoi:istype("Boolean")) {
			set result["eoi"] to eoi[0].
			set rest to eoi[1].
		}
		
		return list(result, rest).
	}).
	fromloopStatementParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"from", func(self:from),
			"from_block", func(self:from_block),
			"until", func(self:until),
			"until_expr", func(self:until_expr),
			"step", func(self:step),
			"step_block", func(self:step_block),
			"do", func(self:do),
			"do_instruction", func(self:do_instruction),
			"eoi", func(self:eoi)
		).
	}).
	fromloopStatementParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self:from, self:from_block, self:until, self:until_expr, self:step, self:step_block, self:do, self:do_instruction, self:eoi)).
	}).
	result:add("fromloop_stmt", fromloopStatementParser).

	// unlock_stmt
	local unlockStatementParser to lexicon().
	unlockStatementParser:add("parse", {
		declare parameter input, definitions, recurse.
		// UNLOCK (IDENTIFIER | ALL) EOI;
		
		local result to lexicon("type", unlockStatementParser).

		local parts to in_order(input, definitions, list("UNLOCK", list("IDENTIFIER", "ALL"), "EOI"), recurse, "unlock_stmt").
		if (parts:istype("Boolean")) {
			return parts.
		}
		set result["unlock"] to parts[0][0].
		set result["identifier"] to parts[0][1].
		set result["eoi"] to parts[0][2].
		
		return list(result, parts[1]).
	}).
	unlockStatementParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"unlock", func(self:unlock),
			"identifier", func(self:identifier),
			"eoi", func(self:eoi)
		).
	}).
	unlockStatementParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self:unlock, self:identifier, self:eoi)).
	}).
	result:add("unlock_stmt", unlockStatementParser).

	// print_stmt
	local printStatementParser to lexicon().
	printStatementParser:add("parse", {
		declare parameter input, definitions, recurse.
		// PRINT expr (AT BRACKETOPEN expr COMMA expr BRACKETCLOSE)? EOI;
		
		local result to lexicon("type", printStatementParser).

		local parts to in_order(input, definitions, list("PRINT", "expr"), recurse, "print_stmt").
		if (parts:istype("Boolean")) {
			return parts.
		}
		set result["print"] to parts[0][0].
		set result["value"] to parts[0][1].
		
		set result["to"] to false.
		set result["open"] to false.
		set result["x"] to false.
		set result["comma"] to false.
		set result["y"] to false.
		set result["close"] to false.
		local rest to parts[1].
		local position to in_order(rest, definitions, list("AT", "BRACKETOPEN", "expr", "COMMA", "expr", "BRACKETCLOSE"), recurse, "print_stmt").
		if (not position:istype("Boolean")) {
			set result["to"] to position[0][0].
			set result["open"] to position[0][1].
			set result["x"] to position[0][2].
			set result["comma"] to position[0][3].
			set result["y"] to position[0][4].
			set result["close"] to position[0][5].
			set rest to position[1].
		}
		
		local eoi to recurse(rest, definitions, "EOI").
		if (eoi:istype("Boolean")) {
			print("Expected '.' when parsing print_stmt. Instead I found: " + debug_next_symbol(rest)).
			return true.
		}
		set result["eoi"] to eoi[0].
		
		return list(result, eoi[1]).
	}).
	printStatementParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"print", func(self:print),
			"value", func(self:value),
			"to", func(self:to),
			"open", func(self:open),
			"x", func(self:x),
			"comma", func(self:comma),
			"y", func(self:y),
			"close", func(self:close),
			"eoi", func(self:eoi)
		).
	}).
	printStatementParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self:print, self:value, self:to, self:open, self:x, self:comma, self:y, self:close, self:eoi)).
	}).
	result:add("print_stmt", printStatementParser).

	// on_stmt
	local onStatementParser to lexicon().
	onStatementParser:add("parse", {
		declare parameter input, definitions, recurse.
		// ON varidentifier instruction EOI?;
		
		local result to lexicon("type", onStatementParser).

		local parts to in_order(input, definitions, list("ON", "varidentifier", "instruction"), recurse, "on_stmt").
		if (parts:istype("Boolean")) {
			return parts.
		}
		set result["on"] to parts[0][0].
		set result["variable"] to parts[0][1].
		set result["instruction"] to parts[0][2].
		set result["eoi"] to false.
		
		local rest to parts[1].
		local eoi to recurse(rest, definitions, "EOI").
		if (not eoi:istype("Boolean")) {
			set result["eoi"] to eoi[0].
			set rest to eoi[1].
		}
		
		return list(result, rest).
	}).
	onStatementParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"on", func(self:on),
			"variable", func(self:variable),
			"instruction", func(self:instruction),
			"eoi", func(self:eoi)
		).
	}).
	onStatementParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self:on, self:variable, self:instruction, self:eoi)).
	}).
	result:add("on_stmt", onStatementParser).

	// toggle_stmt
	local toggleStatementParser to lexicon().
	toggleStatementParser:add("parse", {
		declare parameter input, definitions, recurse.
		// TOGGLE varidentifier EOI;
		
		local result to lexicon("type", toggleStatementParser).

		local parts to in_order(input, definitions, list("TOGGLE", "varidentifier", "EOI"), recurse, "toggle_stmt").
		if (parts:istype("Boolean")) {
			return parts.
		}
		set result["toggle"] to parts[0][0].
		set result["identifier"] to parts[0][1].
		set result["eoi"] to parts[0][2].
		
		return list(result, parts[1]).
	}).
	toggleStatementParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"toggle", func(self:toggle),
			"identifier", func(self:identifier),
			"eoi", func(self:eoi)
		).
	}).
	toggleStatementParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self:toggle, self:identifier, self:eoi)).
	}).
	result:add("toggle_stmt", toggleStatementParser).

	// wait_stmt
	local waitStatementParser to lexicon().
	waitStatementParser:add("parse", {
		declare parameter input, definitions, recurse.
		// WAIT UNTIL? expr EOI;
		
		local result to lexicon("type", waitStatementParser).

		local parts to in_order(input, definitions, list("WAIT", "?UNTIL", "expr", "EOI"), recurse, "wait_stmt").
		if (parts:istype("Boolean")) {
			return parts.
		}
		set result["wait"] to parts[0][0].
		set result["until"] to parts[0][1].
		set result["expr"] to parts[0][2].
		set result["eoi"] to parts[0][3].
		
		return list(result, parts[1]).
	}).
	waitStatementParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"wait", func(self:wait),
			"until", func(self:until),
			"expr", func(self:expr),
			"eoi", func(self:eoi)
		).
	}).
	waitStatementParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self:wait, self:until, self:expr, self:eoi)).
	}).
	result:add("wait_stmt", waitStatementParser).
	
	// when_stmt
	local whenStatementParser to lexicon().
	whenStatementParser:add("parse", {
		declare parameter input, definitions, recurse.
		// WHEN expr THEN instruction EOI?;
		
		local result to lexicon("type", whenStatementParser).

		local parts to in_order(input, definitions, list("WHEN", "expr", "THEN", "instruction", "?EOI"), recurse, "when_stmt").
		if (parts:istype("Boolean")) {
			return parts.
		}
		set result["when"] to parts[0][0].
		set result["expr"] to parts[0][1].
		set result["then"] to parts[0][2].
		set result["instruction"] to parts[0][3].
		set result["eoi"] to parts[0][4].
		
		return list(result, parts[1]).
	}).
	whenStatementParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"when", func(self:when),
			"expr", func(self:expr),
			"then", func(self:then),
			"instruction", func(self:instruction),
			"eoi", func(self:eoi)
		).
	}).
	whenStatementParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self:when, self:expr, self:then, self:instruction, self:eoi)).
	}).
	result:add("when_stmt", whenStatementParser).
	
	// onoff_stmt
	local onoffStatementParser to lexicon().
	onoffStatementParser:add("parse", {
		declare parameter input, definitions, recurse.
		// varidentifier onoff_trailer EOI;
		
		local result to lexicon("type", onoffStatementParser).

		local parts to in_order(input, definitions, list("varidentifier", "onoff_trailer", "EOI"), recurse, "onoff_stmt").
		if (parts:istype("Boolean")) {
			return parts.
		}
		set result["identifier"] to parts[0][0].
		set result["onoff"] to parts[0][1].
		set result["eoi"] to parts[0][2].
		
		return list(result, parts[1]).
	}).
	onoffStatementParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"identifier", func(self:identifier),
			"onoff", func(self:onoff),
			"eoi", func(self:eoi)
		).
	}).
	onoffStatementParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self:identifier, self:onoff, self:eoi)).
	}).
	result:add("onoff_stmt", onoffStatementParser).
	
	// onoff_trailer
	local onoffTrailerParser to lexicon().
	onoffTrailerParser:add("parse", {
		declare parameter input, definitions, recurse.
		// (ON | OFF);
		
		local result to lexicon("type", onoffTrailerParser).

		local value to one_of(input, definitions, list("ON", "OFF"), recurse).
		if (value:istype("Boolean")) {
			return value.
		}
		set result["value"] to value[0].
		
		return list(result, value[1]).
	}).
	onoffTrailerParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"value", func(self:value)
		).
	}).
	onoffTrailerParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self:value)).
	}).
	result:add("onoff_trailer", onoffTrailerParser).

	// stage_stmt
	local stageStatementParser to lexicon().
	stageStatementParser:add("parse", {
		declare parameter input, definitions, recurse.
		// STAGE EOI;
		
		local result to lexicon("type", stageStatementParser).

		local parts to in_order(input, definitions, list("STAGE", "EOI"), recurse, "stage_stmt").
		if (parts:istype("Boolean")) {
			return parts.
		}
		set result["stage"] to parts[0][0].
		set result["eoi"] to parts[0][1].
		
		return list(result, parts[1]).
	}).
	stageStatementParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"stage", func(self:stage),
			"eoi", func(self:eoi)
		).
	}).
	stageStatementParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self:stage, self:eoi)).
	}).
	result:add("stage_stmt", stageStatementParser).
	
	// clear_stmt
	local clearStatementParser to lexicon().
	clearStatementParser:add("parse", {
		declare parameter input, definitions, recurse.
		// CLEARSCREEN EOI;
		
		local result to lexicon("type", clearStatementParser).

		local parts to in_order(input, definitions, list("CLEARSCREEN", "EOI"), recurse, "clear_stmt").
		if (parts:istype("Boolean")) {
			return parts.
		}
		set result["clear"] to parts[0][0].
		set result["eoi"] to parts[0][1].
		
		return list(result, parts[1]).
	}).
	clearStatementParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"clear", func(self:clear),
			"eoi", func(self:eoi)
		).
	}).
	clearStatementParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self:clear, self:eoi)).
	}).
	result:add("clear_stmt", clearStatementParser).
	
	// add_stmt
	local addStatementParser to lexicon().
	addStatementParser:add("parse", {
		declare parameter input, definitions, recurse.
		// ADD expr EOI;
		
		local result to lexicon("type", addStatementParser).

		local parts to in_order(input, definitions, list("ADD", "expr", "EOI"), recurse, "add_stmt").
		if (parts:istype("Boolean")) {
			return parts.
		}
		set result["add"] to parts[0][0].
		set result["expr"] to parts[0][1].
		set result["eoi"] to parts[0][2].
		
		return list(result, parts[1]).
	}).
	addStatementParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"add", func(self["add"]),
			"expr", func(self:expr),
			"eoi", func(self:eoi)
		).
	}).
	addStatementParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self["add"], self:expr, self:eoi)).
	}).
	result:add("add_stmt", addStatementParser).
	
	// remove_stmt
	local removeStatementParser to lexicon().
	removeStatementParser:add("parse", {
		declare parameter input, definitions, recurse.
		// REMOVE expr EOI;
		
		local result to lexicon("type", removeStatementParser).

		local parts to in_order(input, definitions, list("REMOVE", "expr", "EOI"), recurse, "remove_stmt").
		if (parts:istype("Boolean")) {
			return parts.
		}
		set result["rem_stmt"] to parts[0][0].
		set result["expr"] to parts[0][1].
		set result["eoi"] to parts[0][2].
		
		return list(result, parts[1]).
	}).
	removeStatementParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"rem_stmt", func(self:rem_stmt),
			"expr", func(self:expr),
			"eoi", func(self:eoi)
		).
	}).
	removeStatementParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self:rem_stmt, self:expr, self:eoi)).
	}).
	result:add("remove_stmt", removeStatementParser).
	
	// log_stmt
	local logStatementParser to lexicon().
	logStatementParser:add("parse", {
		declare parameter input, definitions, recurse.
		// LOG expr TO expr EOI;
		
		local result to lexicon("type", logStatementParser).

		local parts to in_order(input, definitions, list("LOG", "expr", "TO", "expr", "EOI"), recurse, "log_stmt").
		if (parts:istype("Boolean")) {
			return parts.
		}
		set result["log"] to parts[0][0].
		set result["expr"] to parts[0][1].
		set result["to"] to parts[0][2].
		set result["file"] to parts[0][3].
		set result["eoi"] to parts[0][4].
		
		return list(result, parts[1]).
	}).
	logStatementParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"log", func(self:log),
			"expr", func(self:expr),
			"to", func(self:to),
			"file", func(self:file),
			"eoi", func(self:eoi)
		).
	}).
	logStatementParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self:log, self:expr, self:to, self:file, self:eoi)).
	}).
	result:add("log_stmt", logStatementParser).
	
	// break_stmt
	local breakStatementParser to lexicon().
	breakStatementParser:add("parse", {
		declare parameter input, definitions, recurse.
		// BREAK EOI;
		
		local result to lexicon("type", breakStatementParser).

		local parts to in_order(input, definitions, list("BREAK", "EOI"), recurse, "break_stmt").
		if (parts:istype("Boolean")) {
			return parts.
		}
		set result["break"] to parts[0][0].
		set result["eoi"] to parts[0][1].
		
		return list(result, parts[1]).
	}).
	breakStatementParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"break", func(self:break),
			"eoi", func(self:eoi)
		).
	}).
	breakStatementParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self:break, self:eoi)).
	}).
	result:add("break_stmt", breakStatementParser).
	
	// preserve_stmt
	local preserveStatementParser to lexicon().
	preserveStatementParser:add("parse", {
		declare parameter input, definitions, recurse.
		// PRESERVE EOI;
		
		local result to lexicon("type", preserveStatementParser).

		local parts to in_order(input, definitions, list("PRESERVE", "EOI"), recurse, "preserve_stmt").
		if (parts:istype("Boolean")) {
			return parts.
		}
		set result["preserve"] to parts[0][0].
		set result["eoi"] to parts[0][1].
		
		return list(result, parts[1]).
	}).
	preserveStatementParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"preserve", func(self:preserve),
			"eoi", func(self:eoi)
		).
	}).
	preserveStatementParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self:preserve, self:eoi)).
	}).
	result:add("preserve_stmt", preserveStatementParser).
	
	// declare_identifier_clause
	local declareIdentifierClauseParser to lexicon().
	declareIdentifierClauseParser:add("parse", {
		declare parameter input, definitions, recurse.
		// IDENTIFIER (TO|IS) expr EOI;
		
		local result to lexicon("type", declareIdentifierClauseParser).

		local parts to in_order(input, definitions, list(valid_identifiers, list("TO", "IS"), "expr", "EOI"), recurse, "declare_identifier_clause").
		if (parts:istype("Boolean")) {
			return parts.
		}
		set result["identifier"] to parts[0][0].
		set result["tois"] to parts[0][1].
		set result["expr"] to parts[0][2].
		set result["eoi"] to parts[0][3].
		
		return list(result, parts[1]).
	}).
	declareIdentifierClauseParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"identifier", func(self:identifier),
			"tois", func(self:tois),
			"expr", func(self:expr),
			"eoi", func(self:eoi)
		).
	}).
	declareIdentifierClauseParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self:identifier, self:tois, self:expr, self:eoi)).
	}).
	result:add("declare_identifier_clause", declareIdentifierClauseParser).
	
	// declare_parameter_clause
	local declareParameterClauseParser to lexicon().
	declareParameterClauseParser:add("parse", {
		declare parameter input, definitions, recurse.
		// PARAMETER IDENTIFIER ((TO|IS) expr)? (COMMA IDENTIFIER ((TO|IS) expr)?)* EOI;
		
		local result to lexicon("type", declareParameterClauseParser).

		local prefix to in_order(input, definitions, list("PARAMETER", valid_identifiers), recurse, "declare_parameter_clause").
		if (prefix:istype("Boolean")) {
			return prefix.
		}
		
		set result["parameter"] to prefix[0][0].
		set result["identifier"] to prefix[0][1].
		
		local rest to prefix[1].
		local value to in_order(rest, definitions, list(list("TO", "IS"), "expr"), recurse, "declare_parameter_clause").
		if (value:istype("Boolean")) {
			set result["to"] to false.
			set result["value"] to false.
		} else {
			set result["to"] to value[0][0].
			set result["value"] to value[0][1].
			set rest to value[1].
		}
		
		local params to list().
		local nextparam to in_order(rest, definitions, list("COMMA", valid_identifiers), recurse, "declare_parameter_clause").
		until nextparam:istype("Boolean") {
			local param to nextparam[0].
			set rest to nextparam[1].
			
			local value to in_order(rest, definitions, list(list("TO", "IS"), "expr"), recurse, "declare_parameter_clause").
			if (value:istype("Boolean")) {
				param:add(false).
				param:add(false).
			} else {
				param:add(value[0][0]).
				param:add(value[0][1]).
				set rest to value[1].
			}

			params:add(param).
			set nextparam to in_order(rest, definitions, list("COMMA", valid_identifiers), recurse, "declare_parameter_clause").
		}
		
		set result["extra"] to params.
		local eoi to recurse(rest, definitions, "EOI").
		if (eoi:istype("Boolean")) {
			print("Expected EOI while parsing declare_parameter_clause. Instead I found: " + debug_next_symbol(rest)).
			return true.
		}
		set result["eoi"] to eoi[0].
		
		return list(result, eoi[1]).
	}).
	declareParameterClauseParser:add("transform", {
		declare parameter self, func.
		local paramMapped to func(self:parameter).
		local identifierMapped to func(self:identifier).
		local toMapped to func(self:to).
		local valueMapped to func(self:value).
		local mapped to list().
		for param in self:extra {
			mapped:add(list(func(param[0]), func(param[1]), func(param[2]), func(param[3]))).
		}
		
		return lexicon(
			"type", self:type,
			"parameter", paramMapped,
			"identifier", identifierMapped,
			"to", toMapped,
			"value", valueMapped,
			"extra", mapped,
			"eoi", func(self:eoi)
		).
	}).
	declareParameterClauseParser:add("to_symbols", {
		declare parameter self.
		
		local parts to list(self:parameter, self:identifier, self:to, self:value).
		for param in self:extra {
			parts:add(param[0]).
			parts:add(param[1]).
			parts:add(param[2]).
			parts:add(param[3]).
		}
		parts:add(self:eoi).
		
		return symbolize(parts).
	}).
	result:add("declare_parameter_clause", declareParameterClauseParser).
	
	// declare_function_clause
	local declareFunctionClauseParser to lexicon().
	declareFunctionClauseParser:add("parse", {
		declare parameter input, definitions, recurse.
		// FUNCTION IDENTIFIER instruction_block EOI?;
		
		local result to lexicon("type", declareFunctionClauseParser).

		local parts to in_order(input, definitions, list("FUNCTION", "IDENTIFIER", "instruction_block", "?EOI"), recurse, "declare_function_clause").
		if (parts:istype("Boolean")) {
			return parts.
		}
		set result["function"] to parts[0][0].
		set result["identifier"] to parts[0][1].
		set result["instruction_block"] to parts[0][2].
		set result["eoi"] to parts[0][3].
		
		return list(result, parts[1]).
	}).
	declareFunctionClauseParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"function", func(self:function),
			"identifier", func(self:identifier),
			"instruction_block", func(self:instruction_block),
			"eoi", func(self:eoi)
		).
	}).
	declareFunctionClauseParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self:function, self:identifier, self:instruction_block, self:eoi)).
	}).
	result:add("declare_function_clause", declareFunctionClauseParser).
	
	// declare_lock_clause
	local declareLockClauseParser to lexicon().
	declareLockClauseParser:add("parse", {
		declare parameter input, definitions, recurse.
		// LOCK IDENTIFIER TO expr EOI;
		
		local result to lexicon("type", declareLockClauseParser).

		local parts to in_order(input, definitions, list("LOCK", "IDENTIFIER", "TO", "expr", "EOI"), recurse, "declare_lock_clause").
		if (parts:istype("Boolean")) {
			return parts.
		}
		set result["lock"] to parts[0][0].
		set result["identifier"] to parts[0][1].
		set result["to"] to parts[0][2].
		set result["expr"] to parts[0][3].
		set result["eoi"] to parts[0][4].
		
		return list(result, parts[1]).
	}).
	declareLockClauseParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"lock", func(self:lock),
			"identifier", func(self:identifier),
			"to", func(self:to),
			"expr", func(self:expr),
			"eoi", func(self:eoi)
		).
	}).
	declareLockClauseParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self:lock, self:identifier, self:to, self:expr, self:eoi)).
	}).
	result:add("declare_lock_clause", declareLockClauseParser).
	
	// declare_stmt
	local declareStatementParser to lexicon().
	declareStatementParser:add("parse", {
		declare parameter input, definitions, recurse.
		// 
		
		local result to lexicon("type", declareStatementParser).
		local hasModifier to false.
		
		local declare to optional(input, definitions, "DECLARE", recurse).
		set result["declare"] to declare[0].
		set hasModifier to hasModifier or (not declare[0]:istype("Boolean")).
		
		local scope to one_of(declare[1], definitions, list("LOCAL", "GLOBAL", "?"), recurse).
		set result["scope"] to scope[0].
		set hasModifier to hasModifier or (not scope[0]:istype("Boolean")).
		
		local inner to one_of(scope[1], definitions, list("declare_parameter_clause", "declare_function_clause", "declare_lock_clause"), recurse).
		if (inner:istype("Boolean")) {
			if not hasModifier
				return inner.
			
			set inner to recurse(scope[1], definitions, "declare_identifier_clause").
		}
		
		if (inner:istype("Boolean"))
			return inner.
		
		set result["inner"] to inner[0].
		
		return list(result, inner[1]).
	}).
	declareStatementParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"declare", func(self:declare),
			"scope", func(self:scope),
			"inner", func(self:inner)
		).
	}).
	declareStatementParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self:declare, self:scope, self:inner)).
	}).
	result:add("declare_stmt", declareStatementParser).
	
	// return_stmt
	local returnStatementParser to lexicon().
	returnStatementParser:add("parse", {
		declare parameter input, definitions, recurse.
		// RETURN expr? EOI;
		
		local result to lexicon("type", returnStatementParser).

		local parts to in_order(input, definitions, list("RETURN", "?expr", "EOI"), recurse, "return_stmt").
		if (parts:istype("Boolean")) {
			return parts.
		}
		set result["return"] to parts[0][0].
		set result["expr"] to parts[0][1].
		set result["eoi"] to parts[0][2].
		
		return list(result, parts[1]).
	}).
	returnStatementParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"return", func(self:return),
			"expr", func(self:expr),
			"eoi", func(self:eoi)
		).
	}).
	returnStatementParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self:return, self:expr, self:eoi)).
	}).
	result:add("return_stmt", returnStatementParser).
	
	// switch_stmt
	local switchStatementParser to lexicon().
	switchStatementParser:add("parse", {
		declare parameter input, definitions, recurse.
		// SWITCH TO expr EOI;
		
		local result to lexicon("type", switchStatementParser).

		local parts to in_order(input, definitions, list("SWTICH", "TO", "expr", "EOI"), recurse, "switch_stmt").
		if (parts:istype("Boolean")) {
			return parts.
		}
		set result["switch"] to parts[0][0].
		set result["to"] to parts[0][1].
		set result["expr"] to parts[0][2].
		set result["eoi"] to parts[0][3].
		
		return list(result, parts[1]).
	}).
	switchStatementParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"switch", func(self:switch),
			"to", func(self:to),
			"expr", func(self:expr),
			"eoi", func(self:eoi)
		).
	}).
	switchStatementParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self:switch, self:to, self:expr, self:eoi)).
	}).
	result:add("switch_stmt", switchStatementParser).
	
	// copy_stmt
	local copyStatementParser to lexicon().
	copyStatementParser:add("parse", {
		declare parameter input, definitions, recurse.
		// COPY expr (FROM | TO) expr EOI;
		
		local result to lexicon("type", copyStatementParser).

		local parts to in_order(input, definitions, list("COPY", "expr", list("FROM", "TO"), "expr", "EOI"), recurse, "copy_stmt").
		if (parts:istype("Boolean")) {
			return parts.
		}
		set result["copy"] to parts[0][0].
		set result["file"] to parts[0][1].
		set result["direction"] to parts[0][2].
		set result["destination"] to parts[0][3].
		set result["eoi"] to parts[0][4].
		
		return list(result, parts[1]).
	}).
	copyStatementParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"copy", func(self:copy),
			"file", func(self:file),
			"direction", func(self:direction),
			"destination", func(self:destination),
			"eoi", func(self:eoi)
		).
	}).
	copyStatementParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self:copy, self:file, self:direction, self:destination, self:eoi)).
	}).
	result:add("copy_stmt", copyStatementParser).
	
	// rename_stmt
	local renameStatementParser to lexicon().
	renameStatementParser:add("parse", {
		declare parameter input, definitions, recurse.
		// RENAME (VOLUME | FILE)? expr TO expr EOI;
		
		local result to lexicon("type", renameStatementParser).

		local parts to in_order(input, definitions, list("RENAME", list("VOLUME", "FILE", "?"), "expr", "TO", "expr", "EOI"), recurse, "rename_stmt").
		if (parts:istype("Boolean")) {
			return parts.
		}
		set result["rename"] to parts[0][0].
		set result["target_type"] to parts[0][1].
		set result["target"] to parts[0][2].
		set result["to"] to parts[0][3].
		set result["name"] to parts[0][4].
		set result["eoi"] to parts[0][5].
		
		return list(result, parts[1]).
	}).
	renameStatementParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"rename", func(self:rename),
			"target_type", func(self:target_type),
			"target", func(self:target),
			"to", func(self:to),
			"name", func(self:name),
			"eoi", func(self:eoi)
		).
	}).
	renameStatementParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self:copy, self:file, self:direction, self:destination, self:eoi)).
	}).
	result:add("rename_stmt", renameStatementParser).
	
	// delete_stmt
	local deleteStatementParser to lexicon().
	deleteStatementParser:add("parse", {
		declare parameter input, definitions, recurse.
		// DELETE expr (FROM expr)? EOI;
		
		local result to lexicon("type", deleteStatementParser).

		local prefix to in_order(input, definitions, list("DELETE", "expr"), recurse, "delete_stmt").
		if (prefix:istype("Boolean")) {
			return prefix.
		}
		set result["delete"] to prefix[0][0].
		set result["target"] to prefix[0][1].
		
		local rest to prefix[1].
		local volume to in_order(rest, definitions, list("FROM", "expr"), recurse, "delete_stmt"). 
		if volume:istype("Boolean") {
			set result["from"] to false.
			set result["volume"] to false.
		} else {
			set result["from"] to volume[0][0].
			set result["volume"] to volume[0][1].
			set rest to volume[1].
		}
		
		local eoi to recurse(rest, definitions, "EOI").
		if eoi:istype("Boolean") {
			print("Expected '.' when parsing delete_stmt").
			return true.
		}
		set result["eoi"] to eoi[0].
		
		return list(result, eoi[1]).
	}).
	deleteStatementParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"delete", func(self:delete),
			"target", func(self:target),
			"from", func(self:from),
			"volume", func(self:volume),
			"eoi", func(self:eoi)
		).
	}).
	deleteStatementParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self:delete, self:target, self:from, self:volume, self:eoi)).
	}).
	result:add("delete_stmt", deleteStatementParser).
	
	// edit_stmt
	local editStatementParser to lexicon().
	editStatementParser:add("parse", {
		declare parameter input, definitions, recurse.
		// EDIT expr EOI;
		
		local result to lexicon("type", editStatementParser).

		local parts to in_order(input, definitions, list("EDIT", "expr", "EOI"), recurse, "edit_stmt").
		if (parts:istype("Boolean")) {
			return parts.
		}
		set result["edit"] to parts[0][0].
		set result["expr"] to parts[0][1].
		set result["eoi"] to parts[0][2].
		
		return list(result, parts[1]).
	}).
	editStatementParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"edit", func(self:edit),
			"expr", func(self:expr),
			"eoi", func(self:eoi)
		).
	}).
	editStatementParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self:edit, self:expr, self:eoi)).
	}).
	result:add("edit_stmt", editStatementParser).
	
	// run_stmt
	local runStatementParser to lexicon().
	runStatementParser:add("parse", {
		declare parameter input, definitions, recurse.
		// RUN (ONCE)? (FILEIDENT|STRING) (BRACKETOPEN arglist BRACKETCLOSE)? (ON expr)? EOI;
		
		local result to lexicon("type", runStatementParser).

		local prefix to in_order(input, definitions, list("RUN", "?ONCE", list("FILEIDENT", "STRING")), recurse, "run_stmt").
		if (prefix:istype("Boolean")) {
			return prefix.
		}
		set result["run"] to prefix[0][0].
		set result["once"] to prefix[0][1].
		set result["identifier"] to prefix[0][2].
		
		local rest to prefix[1].
		local args to in_order(rest, definitions, list("BRACKETOPEN", "arglist", "BRACKETCLOSE"), recurse, "run_stmt").
		if (args:istype("Boolean")) {
			set result["open"] to false.
			set result["args"] to false.
			set result["close"] to false.
		} else {
			set result["open"] to args[0][0].
			set result["args"] to args[0][1].
			set result["close"] to args[0][2].
			set rest to args[1].
		}
		
		local trigger to in_order(rest, definitions, list("ON", "expr"), recurse, "run_stmt").
		if (trigger:istype("Boolean")) {
			set result["on"] to false.
			set result["trigger"] to false.
		} else {
			set result["on"] to trigger[0][0].
			set result["trigger"] to trigger[0][1].
			set rest to trigger[1].
		}
		
		local eoi to recurse(rest, definitions, "EOI").
		if (eoi:istype("Boolean")) {
			print("Expected '.' while parsing run_stmt").
			return true.
		}
		set result["eoi"] to eoi[0].
		
		return list(result, eoi[1]).
	}).
	runStatementParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"run", func(self:run),
			"once", func(self:once),
			"identifier", func(self:identifier),
			"open", func(self:open),
			"args", func(self:args),
			"close", func(self:close),
			"on", func(self:on),
			"trigger", func(self:trigger),
			"eoi", func(self:eoi)
		).
	}).
	runStatementParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self:run, self:once, self:identifier, self:open, self:args, self:close, self:on, self:trigger, self:eoi)).
	}).
	result:add("run_stmt", runStatementParser).
	
	// runpath_stmt
	local runpathStatementParser to lexicon().
	runpathStatementParser:add("parse", {
		declare parameter input, definitions, recurse.
		// RUNPATH BRACKETOPEN expr (COMMA arglist)? BRACKETCLOSE EOI;
		
		local result to lexicon("type", runpathStatementParser).

		local prefix to in_order(input, definitions, list("RUNPATH", "BRACKETOPEN", "expr"), recurse, "runpath_stmt").
		if (prefix:istype("Boolean")) {
			return prefix.
		}
		set result["runpath"] to prefix[0][0].
		set result["open"] to prefix[0][1].
		set result["name"] to prefix[0][2].
		
		local rest to prefix[1].
		local args to in_order(rest, definitions, list("COMMA", "arglist"), recurse, "runpath_stmt").
		if (args:istype("Boolean")) {
			set result["comma"] to false.
			set result["args"] to false.
		} else {
			set result["comma"] to args[0][0].
			set result["args"] to args[0][1].
			set rest to args[1].
		}
		
		local trigger to in_order(rest, definitions, list("BRACKETCLOSE", "EOI"), recurse, "runpath_stmt").
		if (trigger:istype("Boolean")) {
			print("Expected to find BRACKETCLOSE while parsing runpath_stmt. Instead I found: " + debug_next_symbol(rest)).
			return true.
		}
		set result["close"] to trigger[0][0].
		set result["eoi"] to trigger[0][1].
		
		return list(result, trigger[1]).
	}).
	runpathStatementParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"runpath", func(self:runpath),
			"open", func(self:open),
			"name", func(self:name),
			"comma", func(self:comma),
			"args", func(self:args),
			"close", func(self:close),
			"eoi", func(self:eoi)
		).
	}).
	runpathStatementParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self:runpath, self:open, self:name, self:comma, self:args, self:close, self:eoi)).
	}).
	result:add("runpath_stmt", runpathStatementParser).
	
	// runoncepath_stmt
	local runoncepathStatementParser to lexicon().
	runoncepathStatementParser:add("parse", {
		declare parameter input, definitions, recurse.
		// RUNONCEPATH BRACKETOPEN expr (COMMA arglist)? BRACKETCLOSE EOI;
		
		local result to lexicon("type", runoncepathStatementParser).

		local prefix to in_order(input, definitions, list("RUNONCEPATH", "BRACKETOPEN", "expr"), recurse, "runoncepath_stmt").
		if (prefix:istype("Boolean")) {
			return prefix.
		}
		set result["runoncepath"] to prefix[0][0].
		set result["open"] to prefix[0][1].
		set result["name"] to prefix[0][2].
		
		local rest to prefix[1].
		local args to in_order(rest, definitions, list("COMMA", "arglist"), recurse, "runoncepath_stmt").
		if (args:istype("Boolean")) {
			set result["comma"] to false.
			set result["args"] to false.
		} else {
			set result["comma"] to args[0][0].
			set result["args"] to args[0][1].
			set rest to args[1].
		}
		
		local trigger to in_order(rest, definitions, list("BRACKETCLOSE", "EOI"), recurse, "runoncepath_stmt").
		if (trigger:istype("Boolean")) {
			print("Expected to find BRACKETCLOSE while parsing runoncepath_stmt. Instead I found: " + debug_next_symbol(rest)).
			return true.
		}
		set result["close"] to trigger[0][0].
		set result["eoi"] to trigger[0][1].
		
		return list(result, trigger[1]).
	}).
	runoncepathStatementParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"runoncepath", func(self:runoncepath),
			"open", func(self:open),
			"name", func(self:name),
			"comma", func(self:comma),
			"args", func(self:args),
			"close", func(self:close),
			"eoi", func(self:eoi)
		).
	}).
	runoncepathStatementParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self:runoncepath, self:open, self:expr, self:name, self:comma, self:args, self:close, self:eoi)).
	}).
	result:add("runoncepath_stmt", runoncepathStatementParser).
	
	// compile_stmt
	local compileStatementParser to lexicon().
	compileStatementParser:add("parse", {
		declare parameter input, definitions, recurse.
		// COMPILE expr (TO expr)? EOI;
		
		local result to lexicon("type", compileStatementParser).

		local prefix to in_order(input, definitions, list("COMPILE", "expr"), recurse, "compile_stmt").
		if (prefix:istype("Boolean")) {
			return prefix.
		}
		set result["compile"] to prefix[0][0].
		set result["name"] to prefix[0][1].
		
		local rest to prefix[1].
		local args to in_order(rest, definitions, list("TO", "expr"), recurse, "compile_stmt").
		if (args:istype("Boolean")) {
			set result["to"] to false.
			set result["destination"] to false.
		} else {
			set result["to"] to args[0][0].
			set result["destination"] to args[0][1].
			set rest to args[1].
		}
		
		local eoi to recurse(rest, definitions, "EOI").
		if (eoi:istype("Boolean")) {
			print("Expected '.' when parsing compile_stmt").
			return true.
		}
		set result["eoi"] to eoi[0].
		
		return list(result, eoi[1]).
	}).
	compileStatementParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"compile", func(self:compile),
			"name", func(self:name),
			"to", func(self:to),
			"destination", func(self:destination),
			"eoi", func(self:eoi)
		).
	}).
	compileStatementParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self:compile, self:name, self:to, self:destination, self:eoi)).
	}).
	result:add("compile_stmt", compileStatementParser).
	
	// list_stmt
	local listStatementParser to lexicon().
	listStatementParser:add("parse", {
		declare parameter input, definitions, recurse.
		// LIST (IDENTIFIER (IN IDENTIFIER)?)? EOI;
		
		local result to lexicon("type", listStatementParser).

		local list to recurse(input, definitions, "LIST").
		if list:istype("Boolean") {
			return list.
		}
		set result["list"] to list[0].
		local rest to list[1].
		
		local identifier to recurse(rest, definitions, "IDENTIFIER").
		if identifier:istype("Boolean") {
			set result["filter"] to false.
			set result["in"] to false.
			set result["identifier"] to false.
		} else {
			set result["filter"] to identifier[0].
			set rest to identifier[1].
			
			local destination to in_order(rest, definitions, list("IN", valid_identifiers), recurse, "list_stmt").
			if destination:istype("Boolean") {
				set result["in"] to false.
				set result["identifier"] to false.
			} else {
				set result["in"] to destination[0][0].
				set result["identifier"] to destination[0][1].
				set rest to destination[1].
			}
		}
		
		local eoi to recurse(rest, definitions, "EOI").
		if (eoi:istype("Boolean")) {
			print("Expected '.' when parsing compile_stmt").
			return true.
		}
		set result["eoi"] to eoi[0].
		
		return list(result, eoi[1]).
	}).
	listStatementParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"list", func(self:list),
			"filter", func(self:filter),
			"in", func(self:in),
			"identifier", func(self:identifier),
			"eoi", func(self:eoi)
		).
	}).
	listStatementParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self:list, self:filter, self:in, self:identifier, self:eoi)).
	}).
	result:add("list_stmt", listStatementParser).
	
	// reboot_stmt
	local rebootStatementParser to lexicon().
	rebootStatementParser:add("parse", {
		declare parameter input, definitions, recurse.
		// REBOOT EOI;
		
		local result to lexicon("type", rebootStatementParser).

		local parts to in_order(input, definitions, list("REBOOT", "EOI"), recurse, "reboot_stmt").
		if (parts:istype("Boolean")) {
			return parts.
		}
		set result["reboot"] to parts[0][0].
		set result["eoi"] to parts[0][1].
		
		return list(result, parts[1]).
	}).
	rebootStatementParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"reboot", func(self:reboot),
			"eoi", func(self:eoi)
		).
	}).
	rebootStatementParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self:reboot, self:eoi)).
	}).
	result:add("reboot_stmt", rebootStatementParser).
	
	// shutdown_stmt
	local shutdownStatementParser to lexicon().
	shutdownStatementParser:add("parse", {
		declare parameter input, definitions, recurse.
		// SHUTDOWN EOI;
		
		local result to lexicon("type", shutdownStatementParser).

		local parts to in_order(input, definitions, list("SHUTDOWN", "EOI"), recurse, "shutdown_stmt").
		if (parts:istype("Boolean")) {
			return parts.
		}
		set result["shutdown"] to parts[0][0].
		set result["eoi"] to parts[0][1].
		
		return list(result, parts[1]).
	}).
	shutdownStatementParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"shutdown", func(self:shutdown),
			"eoi", func(self:eoi)
		).
	}).
	shutdownStatementParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self:shutdown, self:eoi)).
	}).
	result:add("shutdown_stmt", shutdownStatementParser).
	
	// for_stmt
	local forStatementParser to lexicon().
	forStatementParser:add("parse", {
		declare parameter input, definitions, recurse.
		// FOR IDENTIFIER IN varidentifier instruction EOI?;
		
		local result to lexicon("type", forStatementParser).

		local parts to in_order(input, definitions, list("FOR", valid_identifiers, "IN", "varidentifier", "instruction", "?EOI"), recurse, "for_stmt").
		if (parts:istype("Boolean")) {
			return parts.
		}
		set result["for"] to parts[0][0].
		set result["identifier"] to parts[0][1].
		set result["in"] to parts[0][2].
		set result["variable"] to parts[0][3].
		set result["instruction"] to parts[0][4].
		set result["eoi"] to parts[0][5].
		
		return list(result, parts[1]).
	}).
	forStatementParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"for", func(self:for),
			"identifier", func(self:identifier),
			"in", func(self:in),
			"variable", func(self:variable),
			"instruction", func(self:instruction),
			"eoi", func(self:eoi)
		).
	}).
	forStatementParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self:for, self:identifier, self:in, self:variable, self:instruction, self:eoi)).
	}).
	result:add("for_stmt", forStatementParser).
	
	// unset_stmt
	local unsetStatementParser to lexicon().
	unsetStatementParser:add("parse", {
		declare parameter input, definitions, recurse.
		// UNSET (IDENTIFIER | ALL) EOI;
		
		local result to lexicon("type", unsetStatementParser).

		local parts to in_order(input, definitions, list("UNSET", list("IDENTIFIER", "ALL"), "EOI"), recurse, "unset_stmt").
		if (parts:istype("Boolean")) {
			return parts.
		}
		set result["unset"] to parts[0][0].
		set result["target"] to parts[0][1].
		set result["eoi"] to parts[0][2].
		
		return list(result, parts[1]).
	}).
	unsetStatementParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"unset", func(self:unset),
			"target", func(self:target),
			"eoi", func(self:eoi)
		).
	}).
	unsetStatementParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self:unset, self:target, self:eoi)).
	}).
	result:add("unset_stmt", unsetStatementParser).
	
	// lazyglobal_directive
	local lazyglobalDirectiveParser to lexicon().
	lazyglobalDirectiveParser:add("parse", {
		declare parameter input, definitions, recurse.
		// ATSIGN LAZYGLOBAL onoff_trailer EOI;
		
		local result to lexicon("type", lazyglobalDirectiveParser).

		local parts to in_order(input, definitions, list("ATSIGN", "LAZYGLOBAL", "onoff_trailer", "EOI"), recurse, "lazyglobal_directive").
		if (parts:istype("Boolean")) {
			return parts.
		}
		set result["atsign"] to parts[0][0].
		set result["lazyglobal"] to parts[0][1].
		set result["onoff"] to parts[0][2].
		set result["eoi"] to parts[0][3].
		
		return list(result, parts[1]).
	}).
	lazyglobalDirectiveParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"atsign", func(self:atsign),
			"lazyglobal", func(self:lazyglobal),
			"onoff", func(self:onoff),
			"eoi", func(self:eoi)
		).
	}).
	lazyglobalDirectiveParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self:atsign, self:lazyglobal, self:onoff, self:eoi)).
	}).
	result:add("lazyglobal_directive", lazyglobalDirectiveParser).
	
	// class_static_method
	local classStaticMethodParser to lexicon().
	classStaticMethodParser:add("parse", {
		declare parameter input, definitions, recurse.
		// FUNC SELF COLON identifier BRACKETOPEN ?arglist BRACKETCLOSE instruction_block
		// Yea, not LL(1) compatible, oh well

		local result to lexicon("type", classStaticMethodParser).

		local func to recurse(input, definitions, "FUNC").
		if func:istype("Boolean") {
			return func.
		}
		set result["func"] to func[0].

		local parts to in_order(func[1], definitions, list("SELF", "COLON", valid_identifiers, "BRACKETOPEN", "?arglist", "BRACKETCLOSE", "instruction_block"), recurse, "class_static_method").
		if (parts:istype("Boolean")) {
			return parts.
		}
		set result["self"] to parts[0][0].
		set result["colon"] to parts[0][1].
		set result["identifier"] to parts[0][2].
		set result["open"] to parts[0][3].
		set result["args"] to parts[0][4].
		set result["close"] to parts[0][5].
		set result["block"] to parts[0][6].
		
		return list(result, parts[1]).
	}).
	classStaticMethodParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"func", func(self:func),
			"self", func(self:self),
			"colon", func(self:colon),
			"identifier", func(self:identifier),
			"open", func(self:open),
			"args", func(self:args),
			"close", func(self:close),
			"block", func(self:block)
		).
	}).
	classStaticMethodParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self:func, self:self, self:colon, self:identifier, self:open, self:args, self:close, self:block)).
	}).
	result:add("class_static_method", classStaticMethodParser).
	
	// class_method
	local classMethodParser to lexicon().
	classMethodParser:add("parse", {
		declare parameter input, definitions, recurse.
		// FUNC identifier BRACKETOPEN ?arglist BRACKETCLOSE instruction_block
		
		local result to lexicon("type", classMethodParser).

		local parts to in_order(input, definitions, list("FUNC", valid_identifiers, "BRACKETOPEN", "?arglist", "BRACKETCLOSE", "instruction_block"), recurse, "class_method").
		if (parts:istype("Boolean")) {
			return parts.
		}
		set result["func"] to parts[0][0].
		set result["identifier"] to parts[0][1].
		set result["open"] to parts[0][2].
		set result["args"] to parts[0][3].
		set result["close"] to parts[0][4].
		set result["block"] to parts[0][5].
		
		return list(result, parts[1]).
	}).
	classMethodParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"func", func(self:func),
			"identifier", func(self:identifier),
			"open", func(self:open),
			"args", func(self:args),
			"close", func(self:close),
			"block", func(self:block)
		).
	}).
	classMethodParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self:func, self:identifier, self:open, self:args, self:close, self:block)).
	}).
	result:add("class_method", classMethodParser).
	
	// class_constructor
	local classConstructorParser to lexicon().
	classConstructorParser:add("parse", {
		declare parameter input, definitions, recurse.
		// INIT BRACKETOPEN ?arglist BRACKETCLOSE instruction_block
		
		local result to lexicon("type", classConstructorParser).

		local parts to in_order(input, definitions, list("INIT", "BRACKETOPEN", "?arglist", "BRACKETCLOSE", "instruction_block"), recurse, "class_constructor").
		if (parts:istype("Boolean")) {
			return parts.
		}
		set result["init"] to parts[0][0].
		set result["open"] to parts[0][1].
		set result["args"] to parts[0][2].
		set result["close"] to parts[0][3].
		set result["block"] to parts[0][4].
		
		return list(result, parts[1]).
	}).
	classConstructorParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"init", func(self:init),
			"open", func(self:open),
			"args", func(self:args),
			"close", func(self:close),
			"block", func(self:block)
		).
	}).
	classConstructorParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self:init, self:open, self:args, self:close, self:block)).
	}).
	result:add("class_constructor", classConstructorParser).
	
	// class_mixin
	local classMixinParser to lexicon().
	classMixinParser:add("parse", {
		declare parameter input, definitions, recurse.
		// MIX IDENTIFIER
		
		local result to lexicon("type", classMixinParser).

		local parts to in_order(input, definitions, list("MIX", valid_identifiers), recurse, "class_mixin").
		if (parts:istype("Boolean")) {
			return parts.
		}
		set result["mix"] to parts[0][0].
		set result["identifier"] to parts[0][1].
		
		return list(result, parts[1]).
	}).
	classMixinParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"mix", func(self:mix),
			"identifier", func(self:identifier)
		).
	}).
	classMixinParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self:mix, self:identifier)).
	}).
	result:add("class_mixin", classMixinParser).
	
	// class_definition
	local classDefinitionParser to lexicon().
	classDefinitionParser:add("parse", {
		declare parameter input, definitions, recurse.
		// CLASS identifier CURLYOPEN class_property* CURLYCLOSE
		
		local result to lexicon("type", classDefinitionParser).

		local rest to input.
		local globalMatch to one_of(input, definitions, LIST("GLOBAL"), recurse).
		if not globalMatch:istype("Boolean") {
			set rest to globalMatch[1].
			set result["global"] to globalMatch[0].
		} else {
			set result["global"] to false.
		}

		local parts to in_order(rest, definitions, list("CLASS", valid_identifiers, "CURLYOPEN"), recurse, "class_definition").
		if (parts:istype("Boolean")) {
			return parts.
		}
		set result["class"] to parts[0][0].
		set result["identifier"] to parts[0][1].
		set result["open"] to parts[0][2].
		set result["properties"] to list().
		set result["constructor"] to false.
		set result["methods"] to list().
		set result["static_methods"] to list().
		set result["mixins"] to list().

		local rest to parts[1].
		local lastmatch to one_of(rest, definitions, LIST("class_constructor", "class_static_method", "class_method", "class_mixin"), recurse).
		until lastmatch:istype("Boolean") {
			set rest to lastmatch[1].
			result:properties:add(lastmatch[0]).
			if lastmatch[0]:type:name = "class_constructor"
				set result["constructor"] to lastmatch[0].
			if lastmatch[0]:type:name = "class_method"
				result:methods:add(lastmatch[0]).
			if lastmatch[0]:type:name = "class_static_method"
				result:static_methods:add(lastmatch[0]).
			if lastmatch[0]:type:name = "class_mixin"
				result:mixins:add(lastmatch[0]).
			
			set lastmatch to one_of(rest, definitions, LIST("class_constructor", "class_static_method", "class_method", "class_mixin"), recurse).
		}
		if lastmatch
			return true.

		local close to recurse(rest, definitions, "CURLYCLOSE").
		if close:istype("Boolean") {
			if not close
				print("Expected CURLYCLOSE while parsing class_definition. Instead I found: " + debug_next_symbol(rest)).
			return true.
		}
		set result["close"] to close[0].
		local state to close[1][1]:copy.
		set state:usedClass to true.
		
		return list(result, list(close[1][0], state)).
	}).
	classDefinitionParser:add("transform", {
		declare parameter self, func.
		
		local result to lexicon(
			"type", self:type,
			"global", func(self:global),
			"class", func(self:class),
			"identifier", func(self:identifier),
			"open", func(self:open),
			"properties", list(),
			"close", func(self:close),
			"constructor", false,
			"methods", list(),
			"static_methods", list(),
			"mixins", list()
		).
		
		for prop in self:properties {
			local converted to func(prop).
			result:properties:add(converted).
			if converted:type:name = "class_constructor"
				set result["constructor"] to converted.
			if converted:type:name = "class_method"
				result:methods:add(converted).
			if converted:type:name = "class_static_method"
				result:static_methods:add(converted).
			if converted:type:name = "class_mixin"
				result:mixins:add(converted).
		}
		return result.
	}).
	classDefinitionParser:add("to_symbols", {
		declare parameter self.
		
		local parts to list(self:global, self:class, self:identifier, self:open).
		for prop in self:properties {
			parts:add(prop).
		}
		parts:add(self:close).
		return symbolize(parts).
	}).
	result:add("class_definition", classDefinitionParser).
	
	// include_stmt
	local includeStatementParser to lexicon().
	includeStatementParser:add("parse", {
		declare parameter input, definitions, recurse.
		// INCLUDE STRING

		local parts to in_order(input, definitions, list("INCLUDE", "STRING"), recurse, "include_stmt").
		if (parts:istype("Boolean")) {
			return parts.
		}
		
		local pathString to parts[0][1]:literal[1].
		local path to pathString:substring(1,pathString:length - 2).

		local infile to open(path).
		if infile:istype("Boolean") {
			print("Unable to include " + path + " while parsing " + debug_next_symbol(input)).
			return true.
		}
		local data to infile:readall():string.
		local lastLine to -1.
		local state to parts[1][1].
		local lexed to false.
		if state:lexCache:haskey(path) {
			set lexed to state:lexCache[path]:copy.
		} else {
			set lexed to _lex(data, path, state:callback, true).
			state:lexCache:add(path, lexed:copy).
			state:linecounts:add(path, data:split(char(10)):length).
		}

		local whitespace to parts[0][0]:whitespace:copy.
		until whitespace:length = 0 {
			lexed:insert(0, whitespace[whitespace:length - 1]).
			whitespace:remove(whitespace:length - 1).
		}

		local curList to lexed.
		until not curList[curList:length - 1]:istype("List") {
			local modified to curList[curList:length - 1]:copy.
			set curList[curList:length - 1] to modified.
			set curList to modified.
		}
		set curList[curList:length - 1] to parts[1][0].

		return list(lexicon(
			"type", includeStatementParser
		), list(lexed, parts[1][1])).
	}).
	includeStatementParser:add("transform", {
		declare parameter self, func.
		
		return self.
	}).
	includeStatementParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list()).
	}).
	result:add("include_stmt", includeStatementParser).
	
	// mundef_stmt
	local mundefStatementParser to lexicon().
	mundefStatementParser:add("parse", {
		declare parameter input, definitions, recurse.
		// MUNDEF IDENTIFIER

		set _noexpansion to true.
		local parts to in_order(input, definitions, list("MUNDEF", valid_identifiers), recurse, "mundef_stmt").
		set _noexpansion to false.
		if (parts:istype("Boolean")) {
			return parts.
		}
		local newEnv to parts[1][1]:copy.
		local newDefines to newEnv:defines:copy.
		set newEnv:defines to newDefines.
		local identifier to parts[0][1]:literal[1].
		newDefines:remove(identifier).

		return list(lexicon(
			"type", mundefStatementParser
		), list(parts[1][0], newEnv)).
	}).
	mundefStatementParser:add("transform", {
		declare parameter self, func.
		
		return self.
	}).
	mundefStatementParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list()).
	}).
	result:add("mundef_stmt", mundefStatementParser).
	
	// mdefine_stmt
	local mdefineStatementParser to lexicon().
	mdefineStatementParser:add("parse", {
		declare parameter input, definitions, recurse.
		// MDEFINE IDENTIFIER expr

		set _noexpansion to true.
		local parts to in_order(input, definitions, list("MDEFINE", valid_identifiers), recurse, "mdefine_stmt").
		set _noexpansion to false.
		if (parts:istype("Boolean")) {
			return parts.
		}
		local exprPart to recurse(parts[1], definitions, "expr").
		if (exprPart:istype("Boolean")) {
			print("Expected expr while parsing mdefine_stmt. Instead I found: " + debug_next_symbol(parts[1])).
			return true.
		}
		local newEnv to exprPart[1][1]:copy.
		local newDefines to newEnv:defines:copy.
		set newEnv:defines to newDefines.
		local identifier to parts[0][1]:literal[1].
		set newDefines[identifier] to exprPart[0].

		return list(lexicon(
			"type", mdefineStatementParser
		), list(exprPart[1][0], newEnv)).
	}).
	mdefineStatementParser:add("transform", {
		declare parameter self, func.
		
		return self.
	}).
	mdefineStatementParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list()).
	}).
	result:add("mdefine_stmt", mdefineStatementParser).
	
	// mifdef_stmt
	local mifdefStatementParser to lexicon().
	mifdefStatementParser:add("parse", {
		declare parameter input, definitions, recurse.
		// MIFDEF IDENTIFIER ... (MELSE ...)? MENDIF

		set _noexpansion to true.
		local parts to in_order(input, definitions, list(list("MIFDEF", "MIFNDEF"), valid_identifiers), recurse, "mifdef_stmt").
		set _noexpansion to false.
		local identifier to false.
		local eat to false.
		if (parts:istype("Boolean")) {
			local endIfParts to in_order(input, definitions, list("MENDIF"), recurse, "mifdef_stmt").
			if not endIfParts:istype("Boolean") {
				local newEnv to endIfParts[1][1]:copy.
				set newEnv:ifStack to newEnv:ifStack:copy.
				if newEnv:ifStack:length = 0 {
					print("Mismatched #endif at: " + debug_next_symbol(input)).
					return false.
				}
				if newEnv:defines:haskey("DEBUG_IFS") {
					print("":padleft(newEnv:ifStack:length * 2) + "#endif [" + newEnv:ifStack:length + "] at: " + debug_next_symbol(input)).
					local start to newEnv:ifStack[0]:literal.
					print("":padleft(newEnv:ifStack:length * 2) + "matching #if at " + start[0] + "{'" + start[1] + "'} at " + start[2] + ":" + start[3] + ", column " + start[4]).
				}
				newEnv:ifStack:remove(0).

				return list(lexicon(
					"type", mifdefStatementParser
				), list(endIfParts[1][0], newEnv)).
			}
			set parts to in_order(input, definitions, list("MELSE"), recurse, "mifdef_stmt").
			if parts:istype("Boolean") {
				return parts.
			}
			set eat to true.
		} else {
			local invert to parts[0][0]:literal[0] = "MIFNDEF".
			local identifier to parts[0][1]:literal[1].
			set eat to not parts[1][1]:defines:haskey(identifier).
			if invert
				set eat to not eat.
			local newEnv to parts[1][1].
			if newEnv:defines:haskey("DEBUG_IFS") {
				print("":padleft(newEnv:ifStack:length * 2) + "#if [" + newEnv:ifStack:length + "] at: " + debug_next_symbol(input)).
				print("":padleft((newEnv:ifStack:length + 1) * 2) + "Eating: " + eat).
			}
		}
		local newEnv to parts[1][1]:copy.
		set newEnv:ifStack to newEnv:ifStack:copy.
		newEnv:ifStack:insert(0, parts[0][0]).
		if not eat {
			return list(lexicon(
				"type", mifdefStatementParser
			), list(parts[1][0], newEnv)).
		}
		set newEnv:eating to true.

		local curSymbol to skip_symbol(parts[1]).
		local ifCount to 0.
		until ifCount = 0 and (curSymbol[0][0] = "MENDIF" or curSymbol[0][0] = "MELSE") {
			if curSymbol[0][0] = "MIFDEF" or curSymbol[0][0] = "MIFNDEF"
				set ifCount to ifCount + 1.
			if curSymbol[0][0] = "MENDIF"
				set ifCount to ifCount - 1.
			set curSymbol to skip_symbol(curSymbol[1]).
			if curSymbol:istype("Boolean") {
				print("EOF while searching for #endif matching: " + debug_next_symbol(input)).
				print("Try debugging with '#define DEBUG_IFS 1'.").
				return true.
			}
		}
		if curSymbol[0][0] = "MENDIF"
			newEnv:ifStack:remove(0).
		
		if newEnv:defines:haskey("DEBUG_IFS") {
			local start to curSymbol[0].
			local next to curSymbol[1].
			print("":padleft(newEnv:ifStack:length * 2) + curSymbol[0][1] + " [" + newEnv:ifStack:length + "] at: " + start[0] + "{'" + start[1] + "'} at " + start[2] + ":" + start[3] + ", column " + start[4]).
			print("":padleft(newEnv:ifStack:length * 2) + "continue with: " + debug_next_symbol(next)).
		}
		newEnv:remove("eating").
		return list(lexicon(
				"type", mifdefStatementParser
			), list(curSymbol[1][0], newEnv)).
	}).
	mifdefStatementParser:add("transform", {
		declare parameter self, func.
		
		return self.
	}).
	mifdefStatementParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list()).
	}).
	result:add("mifdef_stmt", mifdefStatementParser).
	
	// instruction
	local instructionParser to lexicon().
	instructionParser:add("parse", {
		declare parameter input, definitions, recurse.
		// *_instruction
		
		local result to lexicon("type", instructionParser).

		local inner to one_of(input, definitions, list(
			"empty_stmt",
			"set_stmt",
			"if_stmt",
			"until_stmt",
			"fromloop_stmt",
			"unlock_stmt",
			"print_stmt",
			"on_stmt",
			"toggle_stmt",
			"wait_stmt",
			"when_stmt",
			"stage_stmt",
			"clear_stmt",
			"add_stmt",
			"remove_stmt",
			"log_stmt",
			"break_stmt",
			"preserve_stmt",
			"class_definition",
			"declare_stmt",
			"return_stmt",
			"switch_stmt",
			"copy_stmt",
			"rename_stmt",
			"delete_stmt",
			"edit_stmt",
			"run_stmt",
			"runpath_stmt",
			"runoncepath_stmt",
			"compile_stmt",
			"list_stmt",
			"reboot_stmt",
			"shutdown_stmt",
			"for_stmt",
			"unset_stmt",
			"instruction_block",
			"assignment_stmt",
			"var_definition",
			"include_stmt",
			"mdefine_stmt",
			"mundef_stmt",
			"mifdef_stmt",
			"identifier_led_stmt",
			"lazyglobal_directive"
		), recurse).
		if (inner:istype("Boolean")) {
			return inner.
		}
		set result["inner"] to inner[0].

		return list(result, inner[1]).
	}).
	instructionParser:add("transform", {
		declare parameter self, func.
		
		return lexicon(
			"type", self:type,
			"inner", func(self:inner)
		).
	}).
	instructionParser:add("to_symbols", {
		declare parameter self.
		
		return symbolize(list(self:inner)).
	}).
	result:add("instruction", instructionParser).

	// instruction_block
	local instructionBlockParser to lexicon().
	instructionBlockParser:add("parse", {
		declare parameter input, definitions, recurse.
		// CURLYOPEN instruction* CURLYCLOSE;
		
		local result to lexicon("type", instructionBlockParser).

		local open to recurse(input, definitions, "CURLYOPEN").
		if open:istype("Boolean") {
			return open.
		}
		set result["open"] to open[0].
		
		local rest to open[1].
		local instructions to list().
		local instruction to recurse(rest, definitions, "instruction").
		until instruction:istype("Boolean") {
			instructions:add(instruction[0]).
			set rest to instruction[1].
			
			set instruction to recurse(rest, definitions, "instruction").
		}
		if instruction = true
			return true.
		set result["instructions"] to instructions.

		local close to recurse(rest, definitions, "CURLYCLOSE").
		if (close:istype("Boolean")) {
			print("Expected '}' while parsing instruction_block. Instead I found: " + debug_next_symbol(rest)).
			return true.
		}
			
		set result["close"] to close[0].
		
		return list(result, close[1]).
	}).
	instructionBlockParser:add("transform", {
		declare parameter self, func.
		local openMapped to func(self:open).
		local mapped to list().
		for item in self:instructions
			mapped:add(func(item)).
		
		return lexicon(
			"type", self:type,
			"open", openMapped,
			"instructions", mapped,
			"close", func(self:close)
		).
	}).
	instructionBlockParser:add("to_symbols", {
		declare parameter self.
		local parts to list(self:open).
		for instruction in self:instructions {
			parts:add(instruction).
		}
		parts:add(self:close).
		return symbolize(parts).
	}).
	result:add("instruction_block", instructionBlockParser).
	
	// file
	local fileParser to lexicon().
	fileParser:add("parse", {
		declare parameter input, definitions, recurse.
		// (instruction)* EOF;
		
		local result to lexicon("type", fileParser).
		
		local rest to input.
		local instructions to list().
		local instruction to recurse(rest, definitions, "instruction").
		until instruction:istype("Boolean") {
			instructions:add(instruction[0]).
			set rest to instruction[1].
			
			set instruction to recurse(rest, definitions, "instruction").
		}
		if instruction = true
			return true.
		
		set result["instructions"] to instructions.

		local eof to recurse(rest, definitions, "EOF").
		if (eof:istype("Boolean")) {
			print("Expected to be at the end of the file while parsing instruction_block. Instead I found: " + debug_next_symbol(rest)).
			return true.
		}
			
		set result["eof"] to eof[0].
		
		return list(result, eof[1]).
	}).
	fileParser:add("transform", {
		declare parameter self, func.
		
		local mapped to list().
		for item in self:instructions
			mapped:add(func(item)).
		
		return lexicon(
			"type", self:type,
			"instructions", mapped,
			"eof", func(self:eof)
		).
	}).
	fileParser:add("to_symbols", {
		declare parameter self.
		local parts to self:instructions:copy.
		parts:add(self:eof).
		return symbolize(parts).
	}).
	result:add("file", fileParser).

	for parser in result:keys {
		result[parser]:add("name", parser).
	}

	return result.
}

declare function parse {
	declare parameter input, infile is "unknown", startsymbol is "file", callback is { declare parameter stage, file, pos, max. }, defines is lexicon().

	local lexed to _lex(input, infile, callback).
	local lineCount to input:split(char(10)):length.
	local state to lexicon(
		"defines", lexicon(),
		"ifStack", list(),
		"linecounts", lexicon(infile, lineCount),
		"lexCache", lexicon(),
		"callback", callback,
		"usedClass", false
	).
	set state:defines:casesensitive to true.
	for key in defines:keys {
		state:defines:add(key, defines[key]).
	}
	
	local result to parse_symbols(callback, lineCount, list(lexed, state), parse_definitions(), startsymbol).
	if (not result:istype("Boolean")) {
		return result.
	}
	return false.
}

declare function parse_symbols {
	declare parameter callback, maxlines, root_input, root_definitions, root_symbol is "file".


	local recurse to {
		declare parameter input, definitions, symbol is "file".
	
		if (input[0]:length = 0) {
			return false.
		}
		
		// Literals
		if (unchar(symbol[0]) > 64 and unchar(symbol[0]) < 92) {
			local whitespace to list().
			
			if (input[0]:length < 30 and input[0]:length > 0) {
				local lastItem to input[0][input[0]:length - 1].
				if lastItem:istype("List") {
					// Unwrap nested list
					// This is done to improve copy performance. (Slowed down the universe)
					
					local newList to input[0]:sublist(0, input[0]:length - 1).
					for item in lastItem {
						newList:add(item).
					}
					set input to list(newList, input[1]).
				}
			}

			//captial letter
			local symbols to input[0].
			local next to symbols[0].
			local state to input[1].
			if next:istype("Boolean") {
				print("Premature end of file, compiler bug.").
				return true.
			}
			until (next[0] <> "WHITESPACE" and next[0] <> "NEWLINE" and next[0] <> "COMMENTLINE") {
				whitespace:add(next).
				set symbols to symbols:sublist(1, symbols:length - 1).
				set next to symbols[0].
			}

			if (next[0] <> symbol) {
				return false.
			}
			local result to lexicon().
			result:add("type", definitions["__literal"]).
			result:add("literal", next).
			result:add("whitespace", whitespace).
			return list(result, list(symbols:sublist(1, symbols:length - 1), state)).
		}
		
		// Symbols
		if symbol = "instruction" {
			if input[0][0]:istype("List") and input[0][0]:length = 5 and not input[1]:haskey("eating") {
				local file to input[0][0][2].
				local lines to "?".
				if input[1]:linecounts:haskey(file)
					set lines to input[1]:linecounts[file].
				callback("parsing", file, input[0][0][3], lines).
			}
		}
	
		local parser to definitions[symbol].
		local result to parser:parse(input, definitions, recurse).
		if (not result:istype("Boolean")) {
			if (result[0]:type:name <> symbol) {
				print("Error during parsing: expected to find " + symbol + " but found " + result[0]:type:name).
				return true.
			}
			return result.
		}
		
		return result.
	}.

	local result to recurse(root_input, root_definitions, root_symbol).
	if result:istype("Boolean") {
		return false.
	}

	return lexicon("result", result[0], "definitions", root_definitions, "state", result[1][1]).
}

local topDown to {
	declare parameter func, node.
	
	if node:istype("Boolean")
		return node.
	
	local output to node:type:transform(node, topDown:bind(func)).
	return func(output).
}.

declare function test_parser {
	local result to parse("kuniverse:timewarp:warpto(time:seconds + eta:apoapsis - 5).", "test_parser", "instruction").
	local symbols to result:type:to_symbols(result["result"]).
	local result to "".
	for symbol in symbols {
		set result to result + symbol[1].
	}
	print(result).
}

//test_parser().

global kpp_compile to {
	declare parameter path, outfile to false, defines to lexicon(), callback to false.
	
	local infile to open(path).
	local data to infile:readall().
	
	local nameParts to infile:name:split(".").
	local extension to nameParts[nameParts:length - 1].
	if extension <> "kpp" {
		print("Unable to compile this file. Please give me a file with a .kpp extension.").
		return.
	}
	set nameParts[nameParts:length - 1] to "ks".
	local outName to nameParts:join(".").
	if not outfile:istype("Boolean") {
		set outName to outfile.
	}
	print("Compiling: " + outName).
	
	local lastLine to -1.
	local parsed to parse(data:string, path, "file", {
		declare parameter state, file, pos, max.
		if not callback:istype("Boolean")
			callback:call().
		if pos = lastLine
			return.
		set lastLine to pos.
		if mod(pos, 10) = 0
			print(state + " [" + pos + "/" + max + "]: " + file).
	}, defines:copy).
	if parsed:istype("Boolean") {
		print("Parsing failed").
		return false.
	}
	local definitions to parsed["definitions"].
	
	local newLiteral to {
		declare parameter type, value, template, whitespace is false.
		
		if whitespace:istype("Boolean") {
			set whitespace to template:whitespace.
		}
		
		return lexicon(
			"type", definitions["__literal"],
			"literal", list(type, value, template:literal#2, template:literal#3, template:literal#4),
			"whitespace", whitespace
		).
	}.
	
	local converted to topDown({
		declare parameter node.
		
		local space to list("WHITESPACE", " ", "<generated>", -1, -1).

		if node:type:name = "__literal" and node:literal#0 = "EOI" {
			if not callback:istype("Boolean")
				callback:call().
			return newLiteral("EOI", ".", node).
		}
		
		if node:type:name = "assignment_stmt" {
			local strippedIdentifier to replaceWhitespace(node:varidentifier, "WHITESPACE", " ").
			return lexicon(
				"type", definitions["set_stmt"],
				"set", newLiteral("SET", "set", node:assignment, strippedIdentifier[1]),
				"varidentifier", strippedIdentifier[0],
				"to", newLiteral("TO", "to", node:assignment),
				"expr", node:expr,
				"eoi", node:eoi
			).
		}

		if node:type:name = "obj_construction" {
			local args to false.
			if node:args:istype("Boolean") {
				set args to lexicon(
					"type", definitions["arglist"],
					"left", node:expr,
					"parts", list()
				).
			} else {
				set args to node:args:copy.
				local newparts to args:parts:copy.
				newparts:insert(0, list(newLiteral("COMMA", ",", node:open, list()), args:left)).
				set args["parts"] to newparts.
				set args["left"] to replaceWhitespace(node:expr, false, false)[0].
			}
			
			return lexicon(
				"type", definitions["suffix"],
				"suffixterm", node:expr,
				"trailers", list(
					lexicon(
						"type", definitions["suffix_trailer"],
						"colon", newLiteral("COLON", ":", node:new, list()),
						"suffixterm", lexicon(
							"type", definitions["suffixterm"],
							"atom", newLiteral("IDENTIFIER", "__new", node:new, list()),
							"trailers", list(lexicon(
								"type", definitions["function_trailer"],
								"trailer_type", "braces",
								"open", node:open,
								"arglist", args,
								"close", node:close
							))
						)
					)
				)
			).
		}
		
		if node:type:name = "suffix" {
			
			local transformSuffix to {
				declare parameter subject, parts.
				if parts:empty
					return subject.
				local last to parts[parts:length - 1].
				if last:type:name <> "object_trailer" {
					return lexicon(
						"type", node:type,
						"suffixterm", transformSuffix(subject, parts:sublist(0, parts:length - 1)),
						"trailers", list(last)
					).
				}
				if last:suffixterm:trailers:length = 0 or last:suffixterm:trailers[0]:trailer:type:name = "array_trailer" {
					return lexicon(
						"type", node:type,
						"suffixterm", transformSuffix(subject, parts:sublist(0, parts:length - 1)),
						"trailers", list(lexicon(
							"type", definitions["suffix_trailer"],
							"colon", newLiteral("COLON", ":", last:accessor, list()),
							"suffixterm", last:suffixterm 
						))
					).
				}

				local leftHand to transformSuffix(subject, parts:sublist(0, parts:length - 1)).
				local newArgs to list().
				if not last:suffixterm:trailers[0]:trailer:arglist:istype("Boolean") {
					set newArgs to last:suffixterm:trailers[0]:trailer:arglist:parts:copy.
					newArgs:insert(0, list(newLiteral("COMMA", ",", last:suffixterm:trailers[0]:trailer:open, list()), last:suffixterm:trailers[0]:trailer:arglist:left)).
				}
				local strippedSpace to replaceWhitespace(leftHand, false, false).
				if last:unsafe {
					return lexicon(
						"type", definitions["suffix"],
						"suffixterm", leftHand,
						"trailers", list(
							lexicon(
								"type", definitions["suffix_trailer"],
								"colon", newLiteral("COLON", ":", last:suffixterm:trailers[0]:trailer:open, list()),
								"suffixterm", lexicon(
									"type", definitions["suffixterm"],
									"atom", last:suffixterm:atom,
									"trailers", list(
										lexicon(
											"type", definitions["function_trailer"],
											"trailer_type", "braces",
											"open", last:suffixterm:trailers[0]:trailer:open,
											"arglist", lexicon(
												"type", definitions["arglist"],
												"left", strippedSpace[0],
												"parts", newArgs
											),
											"close", last:suffixterm:trailers[0]:trailer:close
										)
									)
								)
							)
						)
					).
				}
				return lexicon(
					"type", definitions["suffix"],
					"suffixterm", lexicon(
						"type", definitions["suffixterm"],
						"atom", lexicon(
							"type", definitions["atom"],
							"atom_type", "braces",
							"open", newLiteral("BRACKETOPEN", "(", last:suffixterm:trailers[0]:trailer:open, strippedSpace[1]),
							"expr", lexicon(
								"type", definitions["instruction_block"],
								"open", newLiteral("CURLYOPEN", "{", last:suffixterm:trailers[0]:trailer:open, list()),
								"instructions", list(
									lexicon(
										"type", definitions["declare_stmt"],
										"declare", false,
										"scope", newLiteral("LOCAL", "local", last:suffixterm:trailers[0]:trailer:open, list()),
										"inner", lexicon(
											"type", definitions["declare_identifier_clause"],
											"identifier", newLiteral("IDENTIFIER", "__lhs", last:suffixterm:trailers[0]:trailer:open, list(space)),
											"tois", newLiteral("TO", "to ", last:suffixterm:trailers[0]:trailer:open, list(space)),
											"expr", strippedSpace[0],
											"eoi", newLiteral("EOI", ".", last:suffixterm:trailers[0]:trailer:open, list())
										)
									),
									lexicon(
										"type", definitions["return_stmt"],
										"return", newLiteral("RETURN", "return", last:suffixterm:trailers[0]:trailer:open, list(space)),
										"expr", lexicon(
											"type", definitions["suffix"],
											"suffixterm", newLiteral("IDENTIFIER", "__lhs", last:suffixterm:trailers[0]:trailer:open, list(space)),
											"trailers", list(
												lexicon(
													"type", definitions["suffix_trailer"],
													"colon", newLiteral("COLON", ":", last:suffixterm:trailers[0]:trailer:open, list()),
													"suffixterm", lexicon(
														"type", definitions["suffixterm"],
														"atom", last:suffixterm:atom,
														"trailers", list(
															lexicon(
																"type", definitions["function_trailer"],
																"trailer_type", "braces",
																"open", last:suffixterm:trailers[0]:trailer:open,
																"arglist", lexicon(
																	"type", definitions["arglist"],
																	"left", newLiteral("IDENTIFIER", "__lhs", last:suffixterm:trailers[0]:trailer:open, list()),
																	"parts", newArgs
																),
																"close", last:suffixterm:trailers[0]:trailer:close
															)
														)
													)
												)
											)
										),
										"eoi", newLiteral("EOI", ".", last:suffixterm:trailers[0]:trailer:close, list())
									)
								),
								"close", newLiteral("CURLYCLOSE", "}", last:suffixterm:trailers[0]:trailer:close, list())
							),
							"close", newLiteral("BRACKETCLOSE", ")", last:suffixterm:trailers[0]:trailer:close, list())
						),
						"trailers", list()
					),
					"trailers", list(lexicon(
						"type", definitions["suffix_trailer"],
						"colon", newLiteral("COLON", ":", last:suffixterm:trailers[0]:trailer:close, list()),
						"suffixterm", newLiteral("IDENTIFIER", "call()", last:suffixterm:trailers[0]:trailer:close, list())
					))
				).
			}.
			return transformSuffix(node:suffixterm, node:trailers).
		}

		if node:type:name = "class_method" or node:type:name = "class_static_method" {
			local instructions to node:block:instructions:copy.
			local extra to list().
			if not node:args:istype("Boolean") {
				set extra to list(list(newLiteral("COMMA", ",", node:open, list()), node:args:left, false, false)).
				for part in node:args:parts {
					extra:add(list(part[0], part[1], false, false)).
				}
			}
			instructions:insert(0, lexicon(
				"type", definitions["declare_stmt"],
				"declare", false,
				"scope", false,
				"inner", lexicon(
					"type", definitions["declare_parameter_clause"],
					"parameter", newLiteral("PARAMETER", "parameter", node:open, list()),
					"identifier", newLiteral("IDENTIFIER", "self", node:open, list(space)),
					"to", false,
					"value", false,
					"extra", extra,
					"eoi", newLiteral("EOI", ".", node:open, list())
				)
			)).
			local dest to newLiteral("IDENTIFIER", "r", node:func, list(space)).
			if node:type:name = "class_method" {
				set dest to lexicon(
					"type", definitions["suffix"],
					"suffixterm", dest,
					"trailers", list(lexicon(
						"type", definitions["suffix_trailer"],
						"colon", newLiteral("COLON", ":", node:func, list()),
						"suffixterm", newLiteral("IDENTIFIER", "prototype", node:func, list())
					))
				).
			}
			return lexicon(
				"type", definitions["set_stmt"],
				"set", newLiteral("SET", "set", node:func),
				"varidentifier", lexicon(
					"type", definitions["suffix"],
					"suffixterm", dest,
					"trailers", list(lexicon(
						"type", definitions["suffix_trailer"],
						"colon", newLiteral("COLON", ":", node:func, list()),
						"suffixterm", replaceWhitespace(node:identifier, false, false)[0]
					))
				),
				"to", newLiteral("TO", "to", node:func, list(space)),
				"expr", lexicon(
					"type", definitions["instruction_block"],
					"open", node:block:open,
					"instructions", instructions,
					"close", node:block:close
				),
				"eoi", newLiteral("EOI", ".", node:func, list())
			).
		}

		if node:type:name = "class_mixin" {
			local strippedMix to clearWhitespace(node:mix).
			local strippedId to clearWhitespace(node:identifier).
			return lexicon(
				"type", definitions["identifier_led_stmt"],
				"expr", lexicon(
					"type", definitions["suffix"],
					"suffixterm", newLiteral("IDENTIFIER", "r", node:mix),
					"trailers", list(
						lexicon(
							"type", definitions["suffix_trailer"],
							"colon", newLiteral("COLON", ":", node:mix, list()),
							"suffixterm", lexicon(
								"type", definitions["suffixterm"],
								"atom", strippedMix,
								"trailers", list(
									lexicon(
										"type", definitions["function_trailer"],
										"trailer_type", "braces",
										"open", newLiteral("BRACKETOPEN", "(", node:mix, list()),
										"arglist", lexicon(
											"type", definitions["arglist"],
											"left", lexicon(
												"type", definitions["suffix"],
												"suffixterm", newLiteral("IDENTIFIER", "r", node:mix, list()),
												"trailers", list(
													lexicon(
														"type", definitions["suffix_trailer"],
														"colon", newLiteral("COLON", ":", node:mix, list()),
														"suffixterm", newLiteral("IDENTIFIER", "prototype", node:mix, list())
													)
												)
											),
											"parts", list(
												list(
													newLiteral("COMMA", ",", node:mix, list()),
													lexicon(
														"type", definitions["suffix"],
														"suffixterm", strippedId,
														"trailers", list(
															lexicon(
																"type", definitions["suffix_trailer"],
																"colon", newLiteral("COLON", ":", node:mix, list()),
																"suffixterm", newLiteral("IDENTIFIER", "prototype", node:mix, list())
															)
														)
													)
												)
											)
										),
										"close", newLiteral("BRACKETCLOSE", ")", node:mix, list())
									)
								)
							)
						)
					)
				),
				"eoi", newLiteral("EOI", ".", node:mix, list())
			).
		}

		if node:type:name = "class_constructor" {
			local quote to "_""_":substring(1, 1).
			local constructor to lexicon(
				"type", definitions["instruction_block"],
				"open", newLiteral("CURLYOPEN", "{", node:open, list()),
				"instructions", list(
					lexicon(
						"type", definitions["declare_stmt"],
						"declare", false,
						"scope", false,
						"inner", lexicon(
							"type", definitions["declare_parameter_clause"],
							"parameter", newLiteral("PARAMETER", "parameter", node:open, list()),
							"identifier", newLiteral("IDENTIFIER", "__cls", node:open, list(space)),
							"to", false,
							"value", false,
							"extra", list(),
							"eoi", newLiteral("EOI", ".", node:open, list())
						)
					),
					lexicon(
						"type", definitions["declare_stmt"],
						"declare", false,
						"scope", newLiteral("LOCAL", "local", node:open, list()),
						"inner", lexicon(
							"type", definitions["declare_identifier_clause"],
							"identifier", newLiteral("IDENTIFIER", "self", node:open, list(space)),
							"tois", newLiteral("TO", "to", node:open, list(space)),
							"expr", lexicon(
								"type", definitions["suffix"],
								"suffixterm", newLiteral("IDENTIFIER", "__cls", node:open, list(space)),
								"trailers", list(
									lexicon(
										"type", definitions["suffix_trailer"],
										"colon", newLiteral("COLON", ":", node:open, list()),
										"suffixterm", newLiteral("IDENTIFIER", "prototype", node:open, list())
									),
									lexicon(
										"type", definitions["suffix_trailer"],
										"colon", newLiteral("COLON", ":", node:open, list()),
										"suffixterm", newLiteral("IDENTIFIER", "copy", node:open, list())
									)
								)
							),
							"eoi", newLiteral("EOI", ".", node:open, list())
						)
					),
					lexicon(
						"type", definitions["set_stmt"],
						"set", newLiteral("SET", "set", node:open, list(space)),
						"varidentifier", lexicon(
							"type", definitions["suffix"],
							"suffixterm", newLiteral("IDENTIFIER", "self", node:open, list(space)),
							"trailers", list(
								lexicon(
									"type", definitions["suffix_trailer"],
									"colon", newLiteral("COLON", ":", node:open, list()),
									"suffixterm", newLiteral("IDENTIFIER", "type", node:open, list())
								)
							)
						),
						"to", newLiteral("TO", "to", node:open, list(space)),
						"expr", newLiteral("IDENTIFIER", "__cls", node:open, list(space)),
						"eoi", newLiteral("EOI", ".", node:open, list())
					)
				),
				"close", newLiteral("CURLYCLOSE", "}", node:close, list())
			).

			if not node:args:istype("Boolean") {
				constructor:instructions[0]:inner:extra:add(list(
					newLiteral("COMMA", ",", node:open, list()),
					node:args:left,
					false,
					false
				)).
				for part in node:args:parts {
					constructor:instructions[0]:inner:extra:add(list(
						part[0],
						part[1],
						false,
						false
					)).
				}
			}
			for instruction in node:block:instructions {
				constructor:instructions:add(instruction).
			}

			constructor:instructions:add(lexicon(
				"type", definitions["return_stmt"],
				"return", newLiteral("RETURN", "return", node:close, list(space)),
				"expr", newLiteral("IDENTIFIER", "self", node:close, list(space)),
				"eoi", newLiteral("EOI", ".", node:close, list())
			)).
			return lexicon(
				"type", definitions["set_stmt"],
				"set", newLiteral("SET", "set", node:init),
				"varidentifier", lexicon(
					"type", definitions["suffix"],
					"suffixterm", newLiteral("IDENTIFIER", "r", node:init, list(space)),
					"trailers", list(lexicon(
						"type", definitions["suffix_trailer"],
						"colon", newLiteral("COLON", ":", node:init, list()),
						"suffixterm", newLiteral("IDENTIFIER", "__new", node:init, list())
					))
				),
				"to", newLiteral("TO", "to", node:init, list(space)),
				"expr", constructor,
				"eoi", newLiteral("EOI", ".", node:close, list())
			).
		}

		if node:type:name = "class_definition" {
			local quote to "_""_":substring(1, 1).
			local class_block to list().
			if node:properties:length > 0 {
				class_block:add(lexicon(
					"type", definitions["declare_stmt"],
					"declare", false,
					"scope", false,
					"inner", lexicon(
						"type", definitions["declare_parameter_clause"],
						"parameter", newLiteral("PARAMETER", "parameter", node:open, list()),
						"identifier", newLiteral("IDENTIFIER", "r", node:open, list(space)),
						"to", false,
						"value", false,
						"extra", list(),
						"eoi", newLiteral("EOI", ".", node:open, list())
					)
				)).
				for prop in node:properties {
					class_block:add(prop).
				}
			}
			local scope to newLiteral("LOCAL", "local", node:class).
			if not node:global:istype("Boolean") {
				set scope to newLiteral("GLOBAL", "global", node:global).
			}
			return lexicon(
				"type", definitions["declare_stmt"],
				"declare", false,
				"scope", scope,
				"inner", lexicon(
					"type", definitions["declare_identifier_clause"],
					"identifier", node:identifier,
					"tois", newLiteral("TO", "to", node:open, list(space)),
					"expr", lexicon(
						"type", definitions["suffix"],
						"suffixterm", newLiteral("IDENTIFIER", "Class", node:open, list(space)),
						"trailers", list(lexicon(
							"type", definitions["suffix_trailer"],
							"colon", newLiteral("COLON", ":", node:open, list()),
							"suffixterm", lexicon(
								"type", definitions["suffixterm"],
								"atom", newLiteral("IDENTIFIER", "__new", node:open, list()),
								"trailers", list(lexicon(
									"type", definitions["function_trailer"],
									"trailer_type", "braces",
									"open", newLiteral("BRACKETOPEN", "(", node:open, list()),
									"arglist", lexicon(
										"type", definitions["arglist"],
										"left", newLiteral("IDENTIFIER", "Class", node:open, list()),
										"parts", list(list(newLiteral("COMMA", ",", node:open, list()), lexicon(
											"type", definitions["instruction_block"],
											"open", node:open,
											"instructions", class_block,
											"close", node:close
										)))
									),
									"close", newLiteral("BRACKETCLOSE", ")", node:close, list())
								))
							))
						)
					),
					"eoi", newLiteral("EOI", ".", node:close, list())
				)
			).
		}
		
		if node:type:name = "var_definition" {
			return lexicon(
				"type", definitions["declare_stmt"],
				"declare", false,
				"scope", newLiteral("IDENTIFIER", "local", node:var),
				"inner", lexicon(
					"type", definitions["declare_identifier_clause"],
					"identifier", replaceWhitespace(node:identifier, "WHITESPACE", " ")[0],
					"tois", newLiteral("TO", "to", node:assignment, list(space)),
					"expr", replaceWhitespace(node:expr, "WHITESPACE", " ")[0],
					"eoi", newLiteral("EOI", ".", node:eoi)
				)
			).
		}
		
		return node.
	}, parsed["result"]).
	local endState to parsed["state"].
	unset parsed.
	
	local symbols to converted:type:to_symbols(converted).
	unset converted.
	
	local outfile to false.
	if exists(outName) {
		set outfile to open(outName).
		outfile:clear().
	} else {
		set outfile to create(outName).
	}

	local quote to "_""_":substring(1, 1).
	if endState:usedClass and not defines:haskey("NO_STDLIB") {
		outfile:write("@lazyglobal off.
"+quote+"kpp 1.1"+quote+".if defined Class{}else{global Class to lexicon("+quote+"prototype"+quote+",lex("+quote+"__new"+quote+",{parameter c.local p to
c:prototype:copy. set p:type to c. return p.},"+quote+"mix"+quote+",{parameter s,m.
for k in m:keys if not k:startsWith("+quote+"__"+quote+")set s[k] to m[k].})).}set Class:__new to{parameter s,b.local r to
s:prototype:copy. set r:prototype to lex().b:call(r). return r.}."+quote+"https://gitlab.com/thexa4/kos-kpp"+quote+".
").
	} else {
		outfile:write("@lazyglobal off.
"+quote+"kpp 1.1"+quote+"."+quote+"https://gitlab.com/thexa4/kos-kpp"+quote+".
").
	}
	
	for symbol in symbols {
		outfile:write(symbol[1]).
	}

	print("Finished compilation").
	return true.
}.

if not infile:istype("Boolean")
	kpp_compile(infile).