#ifndef LEXER_KPP
#define LEXER_KPP 1

#include "0:/kpp/redux/LexerToken.kpp"

#define UNSAFE_ACCESSOR 1

class LexerWindow {
  init(l, offset) {
    self->lexer = l;
    self->offset = offset;
    self->cache = list();
    self->atend = l->atend;
    self->error = false;
  }

  func get(n) {
    var i = self->offset + n;
    until self->lexer->atend or self->cache:length > i {
      self->cache:add(self->lexer->value);
      self->lexer->next();
    }

    if self->cache:length > i {
      return self->cache[i];
    }
    return false;
  }

  func next() {
    if self->atend {
      self->error = self->lexer->error;
      return false;
    }
    var hasNext = self->get(2);
    var result = self:copy;
    result->atend = hasNext:istype("Boolean");
    result->offset = self->offset + 1;
    return result;
  }
}

class Lexer {
  init(defs, domain, text, filename, noeof) {
    self->defs = list();
    for d in defs {
      if d->domain:find(domain) > -1 {
        self->defs:add(d);
      }
    }
    self->remainingText = text;
    self->filename = filename;
    self->lineNumber = 1;
    self->charOffset = 1;
    self->noeof = noeof;
    self->atend = false;
    self->error = false;
    self->window = new LexerWindow(self, 0);
    self->next();
  }

  func next() {
    var t = self->remainingText;
    if t:length = 0 {
      if not self->noeof {
        self->value = new LexerToken("EOF", "", self->filename, self->lineNumber, self->charOffset, false);
        self->noeof = true;
        return;
      }
      self->value = false;
      self->error = self->filename + ": end of file";
      self->atend = true;
      return;
    }

    for d in self->defs {
      var m = d->match(t);
      if m:istype("string") {
        self->remainingText = self->remainingText:remove(0, m:length);
        self->value = new LexerToken(d->id, m, self->filename, self->lineNumber, self->charOffset, d->whitespace);

        self->charOffset = self->charOffset + m:length;
        if (d->id = "NEWLINE") {
          self->lineNumber = self->lineNumber + 1;
          self->charOffset = 1;
        }
        return;
      }
    }
    
    self->error = "Unexpected character found in file " + self->filename + ":" + self->lineNumber + ", char " + self->charOffset + ": '" + self->remainingText:substring(0, min(10, self->remainingText:length)) + "'...";
    self->atend = true;
    self->value = false;
  }
}

#undef UNSAFE_ACCESSOR
#endif