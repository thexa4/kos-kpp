#ifndef FUNCTION_CALL_NODE_KPP
#define FUNCTION_CALL_NODE_KPP 1

#include "0:/kpp/redux/ParserDefs/ParserNode.kpp"
#include "0:/kpp/redux/ParserDefs/ExprBreakupPropagationNode.kpp"
#include "0:/kpp/redux/ParserDefs/LiteralNode.kpp"
#include "0:/kpp/redux/ParserDefs/DeclareStmtNode.kpp"
#include "0:/kpp/redux/ParserDefs/DeclareIdentifierClause.kpp"

class FunctionCallNode {
  mix ParserNode

  func self:name() {
    return "FunctionCall";
  }

  init(parts) {
    self->subject = parts[0];
    self->bopen = parts[1];
    self->args = parts:copy;
    self->args:remove(0);
    self->args:remove(0);
    self->args:remove(self->args:length - 1);
    self->bclose = parts[parts:length - 1];

    self->attributes = lex();
    self->attributes:is_object_call = false;
    if self->subject:type->name() = "StructureAccess" {
      if self->subject->is_object_accessor()
        self->attributes:is_object_call = true;
    }
  }

  func children() {
    var result = self->args:copy;
    result:insert(0, self->subject);
    result:insert(1, self->bopen);
    result:add(self->bclose);
    return result;
  }

  func desugar(context) {
    var call = self;
    var precursors = list();

    if call->subject:type->name() = "ExprBreakupPropagation" {
      for stmt in call->subject->statements {
        precursors:add(stmt);
      }
      var parts = list(call->subject->inner, call->bopen);
      for a in call->args {
        parts:add(a);
      }
      parts:add(call->bclose);
      call = new FunctionCallNode(parts);
      call->attributes = self->attributes;
    }

    var newArgs = list();
    var isModified = false;
    for arg in call->args {
      if arg:type->name() = "ExprBreakupPropagation" {
        isModified = true;

        for stmt in arg->statements {
          precursors:add(stmt);
        }
        newArgs:add(arg->inner);
      } else {
        newArgs:add(arg);
      }
    }
    if isModified {
      var parts = list(call->subject, call->bopen);
      for a in newArgs {
        parts:add(a);
      }
      parts:add(call->bclose);
      call = new FunctionCallNode(parts);
      call->attributes = self->attributes;
    }

    if call->attributes:is_object_call {
      if call->subject:type->name() = "StructureAccess" {
        var shouldCapture = true;
        var variable = call->subject->subject;

        if call->subject->subject:type->name() = "Atom" {
          shouldCapture = false;
        }

        if shouldCapture {
          variable = ExprBreakupPropagationNode->gen_variable();

          var capturedDef = DeclareStmtScopedDeclarationNode->create_local(variable, call->subject->subject);
          precursors:add(capturedDef);
        }

        var functionSubject = StructureAccessNode->create(variable, call->subject->suffix);

        var comma = LiteralNode->create("COMMA", "", ",");

        var variableStripped = LiteralNode->replaceWhitespace(variable, "");
        var parts = list(functionSubject, call->bopen, variableStripped[0]);
        if call->args:length > 0
          parts:add(comma);
        
        for a in call->args {
          parts:add(a);
        }
        parts:add(call->bclose);
        call = new FunctionCallNode(parts);
        call->attributes = self->attributes;
      }
    }
    if precursors:length = 0 {
      return call;
    }

    precursors:insert(0, call);
    return new ExprBreakupPropagationNode(precursors);
  }

  func self:create(subject, args) {
    var parts = list(
      subject,
      LiteralNode->create("BRACKETOPEN", "", "(")
    );
    var first = true;
    for a in args {
      if first {
        first = false;
      } else {
        parts:add(LiteralNode->create("COMMA", "", ","));
      }
      parts:add(a);
    }
    parts:add(LiteralNode->create("BRACKETCLOSE", "", ")"));
    return new FunctionCallNode(parts);
  }
}

#endif