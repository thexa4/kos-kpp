#ifndef FOR_STMT_NODE_KPP
#define FOR_STMT_NODE_KPP 1

#include "0:/kpp/redux/ParserDefs/SuffixNode.kpp"
#include "0:/kpp/redux/ParserDefs/InstructionNode.kpp"
#include "0:/kpp/redux/ParserDefs/NoneNode.kpp"
#include "0:/kpp/redux/ParserDefs/ParserNode.kpp"

class ForStmtNode {
  mix ParserNode

  func self:name() {
    return "ForStmt";
  }
  func self:format() {
    return list(list("FOR"), VarIdentifiers, list("IN"), list(SuffixNode), list(InstructionNode), list("EOI", NoneNode));
  }

  init(parts) {
    self->forToken = parts[0];
    self->identifier = parts[1];
    self->inToken = parts[2];
    self->collection = parts[3];
    self->instruction = parts[4];
    self->eoi = parts[5];
  }

  func children() {
    return list(self->forToken, self->identifier, self->inToken, self->collection, self->instruction, self->eoi);
  }

  func self:create(identifier, collection, instruction) {
    return new ForStmtNode(list(
      LiteralNode->create("FOR", char(10), "for "),
      identifier,
      LiteralNode->create("IN", " ", "in "),
      collection,
      instruction,
      new NoneNode(list())
    ));
  }

  func desugar(context)
  {
    var arg = self->collection;
    var result = self;
    var propagation = list();

    if arg:type->name() = "ExprBreakupPropagation" {
      propagation = arg->statements:copy;
      arg = arg->inner;
      result = new ForStmtNode(list(self->forToken, self->identifier, self->inToken, arg, self->instruction, self->eoi));
    }

    if self->instruction:type->name() = "NonSingleStatementGuard" {
      var c_open = LiteralNode->create("CURLYOPEN", char(10), "{");
      var c_close = LiteralNode->create("CURLYCLOSE", char(10), "}");
      var block = new InstructionBlockStmtNode(list(c_open, self->instruction->instructions, c_close));
      result = new ForStmtNode(list(self->forToken, self->identifier, self->inToken, arg, block, self->eoi));
    }

    if propagation:length = 0
      return result;
    
    propagation:insert(0, result);
    var breakup = new ExprBreakupPropagationNode(propagation);

    return breakup->to_block(result);
  }
}

#endif