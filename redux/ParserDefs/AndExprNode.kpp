#ifndef AND_EXPR_NODE_KPP
#define AND_EXPR_NODE_KPP 1

#include "0:/kpp/redux/ParserDefs/CompareExprNode.kpp"
#include "0:/kpp/redux/ParserDefs/NoneNode.kpp"
#include "0:/kpp/redux/ParserDefs/ParserNode.kpp"

class AndExprTrailerNode {
  mix ParserNode

  func self:name() {
    return "AndExprTrailer";
  }
  func self:format() {
    return list(list("AND"), list(CompareExprNode), list(AndExprTrailerNode, NoneNode));
  }

  init(parts) {
    self->operator = parts[0];
    self->operand = parts[1];
    self->tail = parts[2];
  }

  func children() {
    return list(self->operator, self->operand, self->tail);
  }

  func desugar(context) {
    var operand = self->operand;
    var tail = self->tail;

    var instructions = list();
    if operand:type->name() = "ExprBreakupPropagation" {
      instructions = operand->statements:copy;
      operand = operand->inner;
    }
    if tail:type->name() = "ExprBreakupPropagation" {
      for i in tail->statements {
        instructions:add(i);
      }
      tail = tail->inner;
    }

    if instructions:length = 0
      return self;
    
    var inner = new AndExprTrailerNode(list(self->operator, operand, tail));
    instructions:insert(0, inner);
    return new ExprBreakupPropagationNode(instructions);
  }
}

class AndExprNode {
  mix ParserNode

  func self:name() {
    return "AndExpr";
  }
  func self:format() {
    return list(list(CompareExprNode), list(AndExprTrailerNode, NoneNode));
  }

  init(parts) {
    self->expr = parts[0];
    self->tail = parts[1];
  }

  func children() {
    return list(self->expr, self->tail);
  }

  func simplify(context) {
    var tailType = self->tail:type;
    if tailType->name() = "None" {
      return self->expr;
    }
    return self;
  }

  func desugar(context) {
    var expr = self->expr;
    var tail = self->tail;

    var instructions = list();
    if expr:type->name() = "ExprBreakupPropagation" {
      instructions = expr->statements:copy;
      expr = expr->inner;
    }
    if tail:type->name() = "ExprBreakupPropagation" {
      for i in tail->statements {
        instructions:add(i);
      }
      tail = tail->inner;
    }

    if instructions:length = 0
      return self;
    
    var inner = new AndExprNode(list(expr, tail));
    instructions:insert(0, inner);
    return new ExprBreakupPropagationNode(instructions);
  }
}

#endif