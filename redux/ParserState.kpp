#ifndef PARSER_STATE
#define PARSER_STATE 1

#include "0:/kpp/redux/LexerDefs.kpp"
#include "0:/kpp/redux/ParserDefs/LiteralNode.kpp"

class ParserState {
  init(pWindow, definitions, nodePrototype) {
    self->window = pWindow;
    self->definitions = definitions;
    self->parts = list();
    self->errors = list();
    self->warnings = list();
    self->matched = false;
    self->fatal = false;
    self->nodePrototype = nodePrototype;
    self->todo = nodePrototype->format():copy;
    self->alternatives = queue();
    self->value = false;
    self->blocked = false;
    self->metaStack = stack();

    for t in self->todo[0] {
      self->alternatives:push(t);
    }

    self->nextToken = false;
    self->eatToken();
  }

  func eatToken() {
    self->prev_window = self->window;

    var whitespace = list();
    self->nextToken = false;

    until(self->window->value:istype("Boolean")) {
      var current = self->window->value;
      if current:istype("Boolean") {
        self->atend = true;
        self->fatal = true;
        print("lexer error");
        self->errors:add(self->window->error);
        return;
      }
      self->window = self->window->next();
      if current->isWhitespace {
        whitespace:add(current);
      } else {
        self->nextToken = new LiteralNode(current, whitespace);
        return;
      }
    }
  }

  func recordFail() {
    var result = self:copy;
    result->alternatives = result->alternatives:copy;
    var lastAlt = result->alternatives:pop();
    if not lastAlt:istype("string") {
      lastAlt = lastAlt->name();
    }

    if result->alternatives:length = 0 {
      result->atend = true;
      if self->parts:length > 0 {
        var todo = self->todo[self->parts:length];
        var error = "Error while trying to parse " + self->nodePrototype->name() + ", expected " + lastAlt + " but reached the end of the file.";
        if not self->nexttoken:istype("Boolean")
          error = "Error while trying to parse " + self->nodePrototype->name() + ", expected " + lastAlt + " but found: " + self->nextToken->debug();
        result = result->with_fatal(error);
      }
    }
    return result;
  }

  func recordMatch(nextState) {
    var result = nextState:copy;
    result->parts = self->parts:copy;
    result->parts:add(result->value);
    result->value = false;
    result->todo = self->todo;
    result->nodePrototype = self->nodePrototype;
    result->matched = false;

    result->alternatives = queue();
    if result->parts:length < result->todo:length {
      for t in result->todo[result->parts:length] {
        result->alternatives:push(t);
      }
    } else {
      result->completeMatch();
    }
    return result;
  }

  func matchToken(id) {
    if self->nextToken:istype("Boolean") {
      return self->recordFail();
    }
    if self->nextToken->token->definition = id {
      var result = self:copy;
      result->parts = self->parts:copy;
      result->parts:add(self->nextToken);
      result->alternatives = queue();
      if result->parts:length < result->todo:length {
        for t in result->todo[result->parts:length] {
          result->alternatives:push(t);
        }
      } else {
        result->completeMatch();
      }
      result->eatToken();
      return result;
    } else {
      return self->recordFail();
    }
  }

  func completeMatch() {
    self->matched = true;
    var prototype = self->nodePrototype;
    //print("matched: " + prototype->name());
    self->value = new prototype(self->parts);
  }

  func branchTodo() {
    var result = self:copy;
    
    result->nodePrototype = self->nextTodo();
    result->todo = result->nodePrototype->format():copy;
    result->alternatives = queue();
    result->value = false;
    result->parts = list();
    //print("try: " + result->nodePrototype->name());
    //print(result->nextToken);

    if result->todo:length > 0 {
      for t in result->todo[0] {
        result->alternatives:push(t);
      }
    } else {
      result->completeMatch();
    }
    return result;
  }

  func nextTodo() {
    if self->alternatives:length = 0 {
      return false;
    }
    return self->alternatives:peek();
  }

  func currentLexerToken() {
    return self->nextToken;
  }

  func include(lWindow) {
    var result = self:copy;
    result->window = new ParserWindow(lWindow, self->prev_window);
    result->nextToken = false;
    result->eatToken();
    return result;
  }

  func handle_meta() {
    var definition = self->nextToken->token->definition;

    if self->metaStack:length > 0 and definition <> "EOF" {
      var result = self:copy;

      if definition = "MELSE" {
        result->metaStack = self:metaStack:copy;
        var current = result->metaStack:pop();
        result->metaStack:push(not current);
        result->eatToken();
        return result;
      }

      if definition = "MENDIF" {
        result->metaStack = self:metaStack:copy;
        result->metaStack:pop();
        result->eatToken();
        return result;
      }

      var shouldSkip = false;
      for s in self->metaStack {
        shouldSkip = shouldSkip or s;
      }
      
      if shouldSkip {

        if definition = "MIFDEF" or definition = "MIFNDEF" {
          result->metaStack = result->metaStack:copy;
          result->metaStack:push(false);
        }

        result->eatToken();

        return result;
      }
    }

    if definition = "MDEFINE" {
      var result = self:copy;

      result->eatToken();
      var identifier = result->nextToken->token;

      result->eatToken();
      var value = result->nextToken->token;
      if result:definitions:haskey(identifier->text)
        result:definitions:remove(identifier->text);
      result:definitions:add(identifier->text, value);

      result->eatToken();
      return result;
    }
    if definition = "MUNDEF" {
      var result = self:copy;

      result->eatToken();
      var identifier = result->nextToken->token;

      result:definitions:remove(identifier->text);

      result->eatToken();
      return result;
    }
    if definition = "MIFDEF" or definition = "MIFNDEF" {
      var result = self:copy;
      
      result->eatToken();
      var identifier = result->nextToken->token;

      result->metaStack = result->metaStack:copy;
      var shouldSkip = result->definitions:haskey(identifier->text);
      if definition = "MINFDEF"
        shouldSkip = not shouldSkip;
      result->metaStack:push(shouldSkip);

      result->eatToken();
      return result;
    }

    if self->definitions:haskey(self->nextToken->token->text) {
      var result = self:copy;
      result->nextToken = self:nextToken:copy;
      result->nextToken->token = self->definitions[self->nextToken->token->text];
      return result;
    }

    return self;
  }

  func with_fatal(msg) {
    var result = self:copy;
    result->errors = self:errors:copy;
    result->errors:add(msg);
    result->fatal = true;
    return result;
  }

  func skip(n) {
    // copy
    // advance window n
  }

  func define(id, value) {
    // copy
    // add define
  }

  func undefine(id) {
    // copy
    // drop define
  }

  func addWarning(str) {
    // copy
    // add warning
  }
}

#endif