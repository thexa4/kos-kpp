# K++ Language
Always wanted to have better tools for code organisation while writing your [KOS](https://ksp-kos.github.io/KOS/) scripts? K++ comes to your rescue!

## Features
 - Object oriented
 - Compiles your scripts at at most one line per second (giving you plenty of time to stretch your legs)
 - Allows in-flight, offline compilation (assuming you can build a big enough harddisk)
 - Allows use of semicolons as statement terminators (forever poisoning your muscle memory)
 - Shorthand notation for variable definition and assignment (`=` operator support)
 - Will crash on syntax errors before you actually run the program
 - Will sometimes crash even if you don't have any syntax errors
 - Allows interoperability with non-kpp files
 - Has a preprocessor to optimize your program based on your mission
 - Keeps compiled code slightly readable

## Usage
Create a kpp file and save it in your Ships/Script folder as `helloworld.kpp`:

    class HelloWorld {
      init() {
        print("Hello World");
      }
    }
    var foo = new HelloWorld();

Copy [parser.ks](parser.ks) to your Ships/Script folder. Run `run "0:/parser"("0:/helloworld.kpp").` You should now see a `helloworld.ks` file which you can run.

## Full example
    #include "0:/math.kpp"
    
    class EventLoop {
      init() {
        self->services = list();
        self->stopped = true;
        self->callbacks = list();
      }

      func register(service) {
        self->services:add(service);
      }
      func deregister(service) {
        var c = 0;
        for s in self->services {
          if s = service {
            service:remove(c);
            return;
          }
        }
        c = c + 1;
      }

      // Causes callback to be called first thing next loop.
      // Enforces one physics tick delay
      func defer(callback, result) {
        self->callbacks:add(list(callback, result));
      }

      func start() {
        self->stopped = false;
        until self->stopped {
          var clist = self->callbacks;
          self->callbacks = list();
          for c in clist {
            c[0]->call(c[1]);
          }
          for service in self->services
            service:run(service);
          wait 0;
        }
      }

      func stop() {
        self->stopped = true;
      }
    }

## Syntax

# Classes

Classes are defined as follows: `class ClassName {}`. Classes can be constructed with the `new` keyword: `new ClassName()`.

Classes can have a constructor defined inside them:
```
class ClassName {
  init() {
    print("constructed");
  }
}
```

Classes can contain functions:
```
class ClassName {
  func print(foo) {
    print(foo);
  }
}
var b = new ClassName();
b->print(42);
```

Classes can be composed out of multiple classes:
```
class FooBar {
  mix ClassName
}

var c = new FooBar();
c->print(42);
```

Static functions can be added as follows:
```
class StaticContainer {
  func self:test() {
    print("hi");
  }
}
StaticContainer->test();
```